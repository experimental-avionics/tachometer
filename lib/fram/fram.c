#include "fram.h"
#include "esp_log.h"

esp_err_t fram_spi_transaction(spi_device_handle_t handle,
			       spi_transaction_t *trans);

esp_err_t fram_init(fram *dev, spi_host_device_t spi, gpio_num_t cs)
{
	esp_err_t ret;

	spi_device_interface_config_t dev_cfg = { .clock_speed_hz = 20000000,
						  .mode = 0,
						  .spics_io_num = cs,
						  .queue_size = 3,
						  .command_bits = 8,
						  .address_bits = 16 };

	ret = spi_bus_add_device(spi, &dev_cfg, &(dev->bus));
	if (ret != ESP_OK)
		return ret;

	return ESP_OK;
}

// esp_err_t fram_spi_transaction(spi_device_handle_t handle,
// 			       spi_transaction_t *trans)
// {
// 	esp_err_t err;

// 	err = spi_device_queue_trans(handle, trans, portMAX_DELAY);
// 	if (err != ESP_OK)
// 		return err;

// 	err = spi_device_get_trans_result(handle, &trans, portMAX_DELAY);
// 	if (err != ESP_OK)
// 		return err;

// 	return ESP_OK;
// }

esp_err_t fram_write(fram *dev, uint16_t addr, void *val, uint16_t len)
{
	// ESP_LOGD("fram", "write");
	
	assert(len % 4 == 0);
	esp_err_t ret;

	{
		spi_transaction_ext_t trans = {
			.address_bits = 0,
			.base =
				{
					.cmd = FRAM_WREN,
					.length = 0,
					.flags = SPI_TRANS_VARIABLE_ADDR,
				},
		};
		spi_transaction_t *_trans = (spi_transaction_t *)&trans;
		ret = spi_device_transmit(dev->bus, _trans);
		if (ret != ESP_OK) {
			return ret;
		}
	}

	{
		spi_transaction_t trans = {
			.cmd = FRAM_WRITE,
			.addr = addr,
			.tx_buffer = val,
			.length = len * 8,
		};
		ret = spi_device_transmit(dev->bus, &trans);
		if (ret != ESP_OK) {
			return ret;
		}
	}
	
	return ESP_OK;
}

esp_err_t fram_read(fram *dev, uint16_t addr, void *val, uint16_t len)
{
	assert(len % 4 == 0);
	spi_transaction_t trans = {
		.cmd = FRAM_READ,
		.addr = addr,
		.tx_buffer = NULL,
		.rx_buffer = val,
		.length = len * 8,
	};

	return spi_device_transmit(dev->bus, &trans);
}

void DumpHex(const void* data, size_t size) {
	ESP_LOGD("ENTR", "DumpHex");
	char ln[10]  = {0};
	char st[150] = {0};
	char * pt = st;
 	int i,j;
	for (i = 0,j = 0; i < size/4; i++) {
		sprintf(pt,"%08x %c",((uint32_t*)data)[i],0);
		pt += 9;
		if (++j > 7 || (i+1)*4 >= size) {
			j = 0;
			sprintf(ln,"%5d",(i*4)-32);
			ESP_LOGD(ln, "%s", st);
			pt = st;
			st[0] = 0;
		}
	}
	ESP_LOGD("EXIT", "DumpHex");
}
