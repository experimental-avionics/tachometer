#ifndef fram_h
#define fram_h

#include <stdint.h>
#include "driver/spi_master.h"

typedef enum {
    FRAM_WREN = 0b00000110,
    FRAM_WRDI = 0b00000100,
    FRAM_RDSR = 0b00000101,
    FRAM_WRSR = 0b00000001,
    FRAM_READ = 0b00000011,
    FRAM_WRITE = 0b00000010
} fram_opcode_t;

typedef struct {
    spi_device_handle_t bus;
} fram;

esp_err_t fram_init(fram *dev, spi_host_device_t spi, gpio_num_t cs);
esp_err_t fram_write(fram *dev, uint16_t addr, void *val, uint16_t len);
esp_err_t fram_read(fram *dev, uint16_t addr, void *val, uint16_t len);
void DumpHex(const void* data, size_t size);

#endif