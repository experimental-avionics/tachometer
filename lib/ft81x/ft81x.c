#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
#include "ft81x.h"
#include "esp_log.h"

esp_err_t ft81x_init(ft81x *dev, spi_host_device_t spi, gpio_num_t cs,
		     gpio_num_t rst)
{
	esp_err_t ret;

	spi_device_interface_config_t dev_cfg = {
		.clock_speed_hz = 10000000,
		.mode = 0,
		.spics_io_num = cs,
		.queue_size = 3,
		.address_bits = 24,
	};

	ret = spi_bus_add_device(spi, &dev_cfg, &(dev->bus));
	if (ret != ESP_OK)
		return ret;

	dev->rst = rst;
	if ((ret = gpio_set_direction(dev->rst, GPIO_MODE_OUTPUT)) != ESP_OK)
		return ret;
	if ((ret = gpio_set_level(dev->rst, 1)) != ESP_OK)
		return ret;

	return ESP_OK;
}

esp_err_t ft81x_hardware_reset(ft81x *dev)
{
	esp_err_t ret;
	if ((ret = gpio_set_level(dev->rst, 0)) != ESP_OK)
		return ret;

	vTaskDelay(100 / portTICK_PERIOD_MS);

	if ((ret = gpio_set_level(dev->rst, 1)) != ESP_OK)
		return ret;

	vTaskDelay(100 / portTICK_PERIOD_MS);

	return ESP_OK;
}

// esp_err_t ft81x_spi_transaction(spi_device_handle_t handle,
// 				spi_transaction_t *trans)
// {
// 	esp_err_t err;

// 	err = spi_device_queue_trans(handle, trans, portMAX_DELAY);
// 	if (err != ESP_OK)
// 		return err;

// 	err = spi_device_get_trans_result(handle, &trans, portMAX_DELAY);
// 	if (err != ESP_OK)
// 		return err;

// 	return ESP_OK;
// }

esp_err_t ft81x_wr8(ft81x *dev, reg_t addr, uint8_t val)
{
	spi_transaction_t trans = {
		.addr = addr | 0x800000,
		.tx_buffer = &val,
		.length = 8,
	};

	return spi_device_transmit(dev->bus, &trans);
}

esp_err_t ft81x_wr16(ft81x *dev, reg_t addr, uint16_t val)
{
	spi_transaction_t trans = {
		.addr = addr | 0x800000,
		.tx_buffer = &val,
		.length = 16,
	};

	return spi_device_transmit(dev->bus, &trans);
}

esp_err_t ft81x_wr32(ft81x *dev, reg_t addr, uint32_t val)
{
	spi_transaction_t trans = {
		.addr = addr | 0x800000,
		.tx_buffer = &val,
		.length = 32,
	};

	return spi_device_transmit(dev->bus, &trans);
}

esp_err_t ft81x_rd8(ft81x *dev, reg_t addr, uint8_t *val)
{
	typedef uint32_t rx_buf_t;
	rx_buf_t rx_buf;
	uint8_t tx_buf[sizeof(rx_buf_t)] = { 0 };

	spi_transaction_t trans = {
		.addr = addr,
		.tx_buffer = &tx_buf,
		.rx_buffer = &rx_buf,
		.length = sizeof(rx_buf_t) * 8
	};
	esp_err_t ret = spi_device_transmit(dev->bus, &trans);
	*val = (rx_buf >> 8) & 0xFF;
	ESP_LOGV("ft81x", "ft81x_rd8(0x%06X)=0x%02X", addr, *val);
	return ret;
}

esp_err_t ft81x_rd16(ft81x *dev, reg_t addr, uint16_t *val)
{
	typedef uint32_t rx_buf_t;
	rx_buf_t rx_buf;
	uint8_t tx_buf[sizeof(rx_buf_t)] = { 0 };

	spi_transaction_t trans = {
		.addr = addr,
		.tx_buffer = &tx_buf,
		.rx_buffer = &rx_buf,
		.length = sizeof(rx_buf_t) * 8
	};
	esp_err_t ret = spi_device_transmit(dev->bus, &trans);
	*val = (rx_buf >> 8) & 0xFFFF;
	ESP_LOGV("ft81x", "ft81x_rd16(0x%06X)=0x%04X", addr, *val);
	return ret;
}

esp_err_t ft81x_rd32(ft81x *dev, reg_t addr, uint32_t *val)
{
	typedef uint64_t rx_buf_t;
	rx_buf_t rx_buf;
	uint8_t tx_buf[sizeof(rx_buf_t)] = { 0 };

	spi_transaction_t trans = {
		.addr = addr,
		.tx_buffer = &tx_buf,
		.rx_buffer = &rx_buf,
		.length = sizeof(rx_buf_t) * 8
	};
	esp_err_t ret = spi_device_transmit(dev->bus, &trans);
	// *val = rx_buf.data;
	*val = (rx_buf >> 8) & 0xFFFFFFFF;
	ESP_LOGV("ft81x", "ft81x_rd32(0x%06X)=0x%08X", addr, *val);
	return ret;
}

esp_err_t ft81x_host_command(ft81x *dev, host_command_t c, uint8_t p)
{
	spi_transaction_t trans = {
		.addr = ((uint32_t)c << 16) | ((uint32_t)p << 8),
		.length = 0,
	};
	return spi_device_transmit(dev->bus, &trans);
}

esp_err_t ft81x_cmd_queue_full(ft81x *dev)
{
	uint32_t buf;
	ft81x_rd32(dev, REG_CMDB_SPACE, &buf);
	return buf == 0x000 ? ESP_OK : ESP_FAIL;
}

esp_err_t ft81x_cmd_queue_empty(ft81x *dev)
{
	uint32_t buf;
	ft81x_rd32(dev, REG_CMDB_SPACE, &buf);
	return buf >= 0xFFC ? ESP_OK : ESP_FAIL;
}

esp_err_t ft81x_wait_for_queue_empty(ft81x *dev, TickType_t max_delay)
{
	while (ft81x_cmd_queue_empty(dev) != ESP_OK) {
		vTaskDelay(1);
		if (--max_delay == 0) break;
	}
	return max_delay != 0 ? ESP_OK : ESP_ERR_TIMEOUT;
}

esp_err_t ft81x_bulk_cmd(ft81x *dev, uint32_t *p, uint16_t len)
{
	spi_transaction_t trans = {
		.addr = REG_CMDB_WRITE | 0x800000,
		.tx_buffer = (void *)p,
		.length = len * 32,
	};

	return spi_device_transmit(dev->bus, &trans);
}

size_t CMD_DL(uint32_t *p, uint32_t dl)
{
	*p = dl;
	return 1;
}
size_t CMD_DLSTART(uint32_t *p)
{
	*p = 0xFFFFFF00;
	return 1;
}
size_t CMD_DLSWAP(uint32_t *p)
{
	*p = 0xFFFFFF01;
	return 1;
}
size_t CMD_GAUGE(uint32_t *p, int16_t x, int16_t y, int16_t r, uint16_t options,
		 uint16_t major, uint16_t minor, uint16_t val, uint16_t range)
{
	*(p++) = 0xFFFFFF13;
	*(p++) = ((uint32_t)x) | ((uint32_t)y << 16);
	*(p++) = ((uint32_t)r) | ((uint32_t)options << 16);
	*(p++) = ((uint32_t)major) | ((uint32_t)minor << 16);
	*(p++) = ((uint32_t)val) | ((uint32_t)range << 16);
	return 5;
}
size_t CMD_FGCOLOR(uint32_t *p, uint32_t c)
{
	*(p++) = 0xFFFFFF0A;
	*(p++) = c & 0xFFFFFF;
	return 2;
}
size_t CMD_BGCOLOR(uint32_t *p, uint32_t c)
{
	*(p++) = 0xFFFFFF09;
	*(p++) = c & 0xFFFFFF;
	return 2;
}
size_t CMD_GRADCOLOR(uint32_t *p, uint32_t c)
{
	*(p++) = 0xFFFFFF34;
	*(p++) = c & 0xFFFFFF;
	return 2;
}
size_t CMD_NUMBER(uint32_t *p, int16_t x, int16_t y, int16_t font,
		  uint16_t options, int32_t n)
{
	*(p++) = 0xFFFFFF2E;
	*(p++) = ((uint32_t)x) | ((uint32_t)y << 16);
	*(p++) = ((uint32_t)font) | ((uint32_t)options << 16);
	*(p++) = (const uint32_t)n;
	return 4;
}
size_t CMD_TEXT(uint32_t *p, int16_t x, int16_t y, int16_t font,
		uint16_t options, char *s)
{
	uint32_t *_s = (uint32_t *)s;

	*(p++) = 0xFFFFFF0C;
	*(p++) = ((uint32_t)x) | ((uint32_t)y << 16);
	*(p++) = ((uint32_t)font) | ((uint32_t)options << 16);

	bool done = false;
	size_t ret = 3;
	do {
		*p = *_s;

		done = (*_s & 0x000000FF) == 0 || (*_s & 0x0000FF00) == 0 ||
		       (*_s & 0x00FF0000) == 0 || (*_s & 0xFF000000) == 0;

		if (done) {
			if ((*_s & 0x000000FF) == 0)
				*p = 0x00000000;
			else if ((*_s & 0x0000FF00) == 0)
				*p &= 0x000000FF;
			else if ((*_s & 0x00FF0000) == 0)
				*p &= 0x0000FFFF;
		}

		++p;
		++_s;
		++ret;
	} while (!done);

	return ret;
}
size_t CMD_ROMFONT(uint32_t *p, uint32_t font, uint32_t romslot)
{
	*(p++) = 0xFFFFFF3F;
	*(p++) = font;
	*(p++) = romslot;
	return 3;
}
size_t CMD_BUTTON(uint32_t *p, int16_t x, int16_t y, int16_t w, int16_t h,
		  int16_t font, uint16_t options, char *s)
{
	uint32_t *_s = (uint32_t *)s;

	*(p++) = 0xFFFFFF0D;
	*(p++) = ((uint32_t)x) | ((uint32_t)y << 16);
	*(p++) = ((uint32_t)w) | ((uint32_t)h << 16);
	*(p++) = ((uint32_t)font) | ((uint32_t)options << 16);

	bool done = false;
	size_t ret = 4;
	do {
		*p = *_s;

		done = (*_s & 0x000000FF) == 0 || (*_s & 0x0000FF00) == 0 ||
		       (*_s & 0x00FF0000) == 0 || (*_s & 0xFF000000) == 0;

		if (done) {
			if ((*_s & 0x000000FF) == 0)
				*p = 0x00000000;
			else if ((*_s & 0x0000FF00) == 0)
				*p &= 0x000000FF;
			else if ((*_s & 0x00FF0000) == 0)
				*p &= 0x0000FFFF;
		}

		++p;
		++_s;
		++ret;
	} while (!done);

	return ret;
}
size_t CMD_SNAPSHOT2(uint32_t *p, uint32_t fmt, uint32_t ptr, int16_t x,
		     int16_t y, int16_t w, int16_t h)
{
	*(p++) = 0xFFFFFF37;
	*(p++) = fmt;
	*(p++) = ptr;
	*(p++) = ((uint32_t)x) | ((uint32_t)y << 16);
	*(p++) = ((uint32_t)w) | ((uint32_t)h << 16);
	return 5;
}
size_t CMD_SETBITMAP(uint32_t *p, uint32_t addr, uint16_t fmt, uint16_t width,
		     uint16_t height)
{
	*(p++) = 0xFFFFFF43;
	*(p++) = addr;
	*(p++) = ((uint32_t)fmt) | ((uint32_t)width << 16);
	*(p++) = (uint32_t)height;
	return 4;
}
size_t CMD_SPINNER(uint32_t *p, int16_t x, int16_t y, uint16_t style,
		   uint16_t scale)
{
	*(p++) = 0xFFFFFF16;
	*(p++) = ((uint32_t)x) | ((uint32_t)y << 16);
	*(p++) = ((uint32_t)style) | ((uint32_t)scale << 16);
	return 3;
}
size_t CMD_CALIBRATE(uint32_t *p)
{
	*(p++) = 0xFFFFFF15;
	*(p++) = 0x000000FF;
	return 2;
}

size_t CMD_SETROTATE(uint32_t *p, uint32_t r)
{
	*(p++) = 0xFFFFFF36;
	*(p++) = r;
	return 2;
}

size_t CMD_MEMWRITE(uint32_t *p, reg_t addr, uint32_t val)
{
	*(p++) = 0xFFFFFF1A;
	*(p++) = addr;
	*(p++) = 4;
	*(p++) = val;
	return 4;
}
