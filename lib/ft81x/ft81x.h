#ifndef ft81x_h
#define ft81x_h

#include <stdint.h>
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

// Host commands
typedef enum host_command_t {
	ACTIVE = 0x00,
	STANDBY = 0x41,
	SLEEP = 0x42,
	PWRDOWN = 0x43, // Also 0x50?
	PD_ROMS = 0x49,
	PD_ROMS_ROM_MAIN = 0x80,
	PD_ROMS_ROM_RCOSATAN = 0x40,
	PD_ROMS_ROM_SAMPLE = 0x20,
	PD_ROMS_ROM_JABOOT = 0x10,
	PD_ROMS_ROM_J1BOOT = 0x08,
	CLKEXT = 0x44,
	CLKINT = 0x48,
	CLKSEL = 0x61, // Also 0x62?
	CLKSEL_DEFAULT = 0,
	RST_PULSE = 0x68,
} host_command_t;

// Memory addresses
typedef enum memory_map_t {
	RAM_G = 0x000000,
	ROM_FONT = 0x1E0000,
	ROM_FONT_ADDR = 0x2FFFFC,
	RAM_DL = 0x300000,
	RAM_REG = 0x302000,
	RAM_CMD = 0x308000,
} memory_map_t;

// Registers
typedef enum reg_t {
	REG_ID = 0x302000,
	REG_FRAMES = 0x302004,
	REG_CLOCK = 0x302008,
	REG_FREQUENCY = 0x30200C,
	REG_RENDERMODE = 0x302010,
	REG_SNAPY = 0x302014,
	REG_SNAPSHOT = 0x302018,
	REG_SNAPFORMAT = 0x30201C,
	REG_CPURESET = 0x302020,
	REG_TAP_CRC = 0x302024,
	REG_TAP_MASK = 0x302028,
	REG_HCYCLE = 0x30202C,
	REG_HOFFSET = 0x302030,
	REG_HSIZE = 0x302034,
	REG_HSYNC0 = 0x302038,
	REG_HSYNC1 = 0x30203C,
	REG_VCYCLE = 0x302040,
	REG_VOFFSET = 0x302044,
	REG_VSIZE = 0x302048,
	REG_VSYNC0 = 0x30204C,
	REG_VSYNC1 = 0x302050,
	REG_DLSWAP = 0x302054,
	DLSWAP_LINE = 1,
	DLSWAP_FRAME = 2,
	REG_ROTATE = 0x302058,
	REG_OUTBITS = 0x30205C,
	REG_DITHER = 0x302060,
	REG_SWIZZLE = 0x302064,
	REG_CSPREAD = 0x302068,
	REG_PCLK_POL = 0x30206C,
	REG_PCLK = 0x302070,
	REG_TAG_X = 0x302074,
	REG_TAG_Y = 0x302078,
	REG_TAG = 0x30207C,
	REG_VOL_PB = 0x302080,
	REG_VOL_SOUND = 0x302084,
	REG_SOUND = 0x302088,
	REG_PLAY = 0x30208C,
	REG_GPIO_DIR = 0x302090,
	REG_GPIO = 0x302094,
	REG_GPIOX_DIR = 0x302098,
	REG_GPIOX = 0x30209C,
	REG_INT_FLAGS = 0x3020A8,
	REG_INT_EN = 0x3020AC,
	REG_INT_MASK = 0x3020B0,
	REG_PLAYBACK_START = 0x3020B4,
	REG_PLAYBACK_LENGTH = 0x3020B8,
	REG_PLAYBACK_READPTR = 0x3020BC,
	REG_PLAYBACK_FREQ = 0x3020C0,
	REG_PLAYBACK_FORMAT = 0x3020C4,
	REG_PLAYBACK_LOOP = 0x3020C8,
	REG_PLAYBACK_PLAY = 0x3020CC,
	REG_PWM_HZ = 0x3020D0,
	REG_PWM_DUTY = 0x3020D4,
	REG_MACRO_0 = 0x3020D8,
	REG_MACRO_1 = 0x3020DC,
	REG_CMD_READ = 0x3020F8,
	REG_CMD_WRITE = 0x3020FC,
	REG_CMD_DL = 0x302100,
	REG_TOUCH_MODE = 0x302104,
	REG_TOUCH_ADC_MODE = 0x302108,
	REG_TOUCH_CHARGE = 0x30210C,
	REG_TOUCH_SETTLE = 0x302110,
	REG_TOUCH_OVERSAMPLE = 0x302114,
	REG_TOUCH_RZTHRESH = 0x302118,
	REG_TOUCH_RAW_XY = 0x30211C,
	REG_TOUCH_RZ = 0x302120,
	REG_TOUCH_SCREEN_XY = 0x302124,
	REG_TOUCH_TAG_XY = 0x302128,
	REG_TOUCH_TAG = 0x30212C,
	REG_TOUCH_TAG1_XY = 0x302130,
	REG_TOUCH_TAG1 = 0x302134,
	REG_TOUCH_TAG2_XY = 0x302138,
	REG_TOUCH_TAG2 = 0x30213C,
	REG_TOUCH_TAG3_XY = 0x302140,
	REG_TOUCH_TAG3 = 0x302144,
	REG_TOUCH_TAG4_XY = 0x302148,
	REG_TOUCH_TAG4 = 0x30214C,
	REG_TOUCH_TRANSFORM_A = 0x302150,
	REG_TOUCH_TRANSFORM_B = 0x302154,
	REG_TOUCH_TRANSFORM_C = 0x302158,
	REG_TOUCH_TRANSFORM_D = 0x30215C,
	REG_TOUCH_TRANSFORM_E = 0x302160,
	REG_TOUCH_TRANSFORM_F = 0x302164,
	REG_TOUCH_CONFIG = 0x302168,
	REG_CTOUCH_TOUCH4_X = 0x30216C,
	REG_BIST_EN = 0x302174,
	REG_TRIM = 0x302180,
	REG_ANA_COMP = 0x302184,
	REG_SPI_WIDTH = 0x302188,
	REG_TOUCH_DIRECT_XY = 0x30218C,
	REG_TOUCH_DIRECT_Z1Z2 = 0x302190,
	REG_DATESTAMP = 0x302564,
	REG_CMDB_SPACE = 0x302574,
	REG_CMDB_WRITE = 0x302578,
	CHIP_ID = 0x0C0000,
} reg_t;

// Command options
typedef enum cmd_option_t {
	OPT_DEFAULT = 0,
	OPT_3D = 0,
	OPT_RGB565 = 0,
	OPT_MONO = 1,
	OPT_NODL = 2,
	OPT_FLAT = 256,
	OPT_SIGNED = 256,
	OPT_CENTERX = 512,
	OPT_CENTERY = 1024,
	OPT_CENTER = 1536,
	OPT_RIGHTX = 2048,
	OPT_NOBACK = 4096,
	OPT_NOTICKS = 8192,
	OPT_NOHM = 16384,
	OPT_NOPOINTER = 16384,
	OPT_NOSECS = 32768,
	OPT_NOHANDS = 49152,
	OPT_NOTEAR = 4,
	OPT_FULLSCREEN = 8,
	OPT_MEDIAFIFO = 16,
	OPT_SOUND = 32,
} cmd_option_t;

// Display list commands
#define VERTEX2F(x, y)                                                         \
	((1UL << 30) | (((x)&32767UL) << 15) | (((y)&32767UL) << 0))
#define VERTEX2II(x, y, handle, cell)                                          \
	((2UL << 30) | (((x)&511UL) << 21) | (((y)&511UL) << 12) |             \
	 (((handle)&31UL) << 7) | (((cell)&127UL) << 0))
#define BITMAP_SOURCE(addr) ((1UL << 24) | (((addr)&4194303UL) << 0))
#define CLEAR_COLOR_RGB(red, green, blue)                                      \
	((2UL << 24) | (((red)&255UL) << 16) | (((green)&255UL) << 8) |        \
	 (((blue)&255UL) << 0))
#define TAG(s) ((3UL << 24) | (((s)&255UL) << 0))
#define COLOR_RGB(red, green, blue)                                            \
	((4UL << 24) | (((red)&255UL) << 16) | (((green)&255UL) << 8) |        \
	 (((blue)&255UL) << 0))
#define BITMAP_HANDLE(handle) ((5UL << 24) | (((handle)&31UL) << 0))
#define CELL(cell) ((6UL << 24) | (((cell)&127UL) << 0))
#define BITMAP_LAYOUT(format, linestride, height)                              \
	((7UL << 24) | (((format)&31UL) << 19) |                               \
	 (((linestride)&1023UL) << 9) | (((height)&511UL) << 0))
#define BITMAP_SIZE(filter, wrapx, wrapy, width, height)                       \
	((8UL << 24) | (((filter)&1UL) << 20) | (((wrapx)&1UL) << 19) |        \
	 (((wrapy)&1UL) << 18) | (((width)&511UL) << 9) |                      \
	 (((height)&511UL) << 0))
#define ALPHA_FUNC(func, ref)                                                  \
	((9UL << 24) | (((func)&7UL) << 8) | (((ref)&255UL) << 0))
#define STENCIL_FUNC(func, ref, mask)                                          \
	((10UL << 24) | (((func)&7UL) << 16) | (((ref)&255UL) << 8) |          \
	 (((mask)&255UL) << 0))
#define BLEND_FUNC(src, dst)                                                   \
	((11UL << 24) | (((src)&7UL) << 3) | (((dst)&7UL) << 0))
#define STENCIL_OP(sfail, spass)                                               \
	((12UL << 24) | (((sfail)&7UL) << 3) | (((spass)&7UL) << 0))
#define POINT_SIZE(size) ((13UL << 24) | (((size)&8191UL) << 0))
#define LINE_WIDTH(width) ((14UL << 24) | (((width)&4095UL) << 0))
#define CLEAR_COLOR_A(alpha) ((15UL << 24) | (((alpha)&255UL) << 0))
#define COLOR_A(alpha) ((16UL << 24) | (((alpha)&255UL) << 0))
#define CLEAR_STENCIL(s) ((17UL << 24) | (((s)&255UL) << 0))
#define CLEAR_TAG(s) ((18UL << 24) | (((s)&255UL) << 0))
#define STENCIL_MASK(mask) ((19UL << 24) | (((mask)&255UL) << 0))
#define TAG_MASK(mask) ((20UL << 24) | (((mask)&1UL) << 0))
#define BITMAP_TRANSFORM_A(a) ((21UL << 24) | (((a)&131071UL) << 0))
#define BITMAP_TRANSFORM_B(b) ((22UL << 24) | (((b)&131071UL) << 0))
#define BITMAP_TRANSFORM_C(c) ((23UL << 24) | (((c)&16777215UL) << 0))
#define BITMAP_TRANSFORM_D(d) ((24UL << 24) | (((d)&131071UL) << 0))
#define BITMAP_TRANSFORM_E(e) ((25UL << 24) | (((e)&131071UL) << 0))
#define BITMAP_TRANSFORM_F(f) ((26UL << 24) | (((f)&16777215UL) << 0))
#define SCISSOR_XY(x, y)                                                       \
	((27UL << 24) | (((x)&2047UL) << 11) | (((y)&2047UL) << 0))
#define SCISSOR_SIZE(width, height)                                            \
	((28UL << 24) | (((width)&4095UL) << 12) | (((height)&4095UL) << 0))
#define CALL(dest) ((29UL << 24) | (((dest)&65535UL) << 0))
#define JUMP(dest) ((30UL << 24) | (((dest)&65535UL) << 0))
#define BEGIN(prim) ((31UL << 24) | (((prim)&15UL) << 0))
#define COLOR_MASK(r, g, b, a)                                                 \
	((32UL << 24) | (((r)&1UL) << 3) | (((g)&1UL) << 2) |                  \
	 (((b)&1UL) << 1) | (((a)&1UL) << 0))
#define CLEAR(c, s, t)                                                         \
	((38UL << 24) | (((c)&1UL) << 2) | (((s)&1UL) << 1) | (((t)&1UL) << 0))
#define VERTEX_FORMAT(frac) ((39UL << 24) | (((frac)&7UL) << 0))
#define BITMAP_LAYOUT_H(linestride, height)                                    \
	((40UL << 24) | (((linestride)&3UL) << 2) | (((height)&3UL) << 0))
#define BITMAP_SIZE_H(width, height)                                           \
	((41UL << 24) | (((width)&3UL) << 2) | (((height)&3UL) << 0))
#define PALETTE_SOURCE(addr) ((42UL << 24) | (((addr)&4194303UL) << 0))
#define VERTEX_TRANSLATE_X(x) ((43UL << 24) | (((x)&131071UL) << 0))
#define VERTEX_TRANSLATE_Y(y) ((44UL << 24) | (((y)&131071UL) << 0))
#define NOP() ((45UL << 24))
#define END() ((33UL << 24))
#define SAVE_CONTEXT() ((34UL << 24))
#define RESTORE_CONTEXT() ((35UL << 24))
#define RETURN() ((36UL << 24))
#define MACRO(m) ((37UL << 24) | (((m)&1UL) << 0))
#undef DISPLAY // This was defined in Arduino.h
#define DISPLAY() ((0UL << 24))

// BEGIN options
typedef enum begin_t {
	BITMAPS = 1,
	POINTS = 2,
	LINES = 3,
	LINE_STRIP = 4,
	EDGE_STRIP_R = 5,
	EDGE_STRIP_L = 6,
	EDGE_STRIP_A = 7,
	EDGE_STRIP_B = 8,
	RECTS = 9,
} begin_t;

// BLEND_FUNC options
typedef enum blend_func_t {
	ZERO = 0,
	ONE = 1,
	SRC_ALPHA = 2,
	DST_ALPHA = 3,
	ONE_MINUS_SRC_ALPHA = 4,
	ONE_MINUS_DST_ALPHA = 5,
} blend_func_t;

// BITMAP_LAYOUT options
typedef enum bitmap_layout_t {
	ARGB1555 = 0,
	L1 = 1,
	L4 = 2,
	L8 = 3,
	RGB332 = 4,
	ARGB2 = 5,
	ARGB4 = 6,
	RGB565 = 7,
	TEXT8X8 = 9,
	TEXTVGA = 10,
	BARGRAPH = 11,
	PALETTED565 = 14,
	PALETTED4444 = 15,
	PALETTED8 = 16,
	L2 = 17,
} bitmap_layout_t;

typedef struct {
	spi_device_handle_t bus;
	gpio_num_t rst;
	uint8_t cmd_buf[4096];
} ft81x;

esp_err_t ft81x_init(ft81x *dev, spi_host_device_t spi, gpio_num_t cs,
		     gpio_num_t rst);
esp_err_t ft81x_hardware_reset(ft81x *dev);

esp_err_t ft81x_wr8(ft81x *dev, reg_t addr, uint8_t val);
esp_err_t ft81x_wr16(ft81x *dev, reg_t addr, uint16_t val);
esp_err_t ft81x_wr32(ft81x *dev, reg_t addr, uint32_t val);

esp_err_t ft81x_rd8(ft81x *dev, reg_t addr, uint8_t *val);
esp_err_t ft81x_rd16(ft81x *dev, reg_t addr, uint16_t *val);
esp_err_t ft81x_rd32(ft81x *dev, reg_t addr, uint32_t *val);

esp_err_t ft81x_host_command(ft81x *dev, host_command_t c, uint8_t p);

esp_err_t ft81x_cmd_queue_full(ft81x *dev);
esp_err_t ft81x_cmd_queue_empty(ft81x *dev);

esp_err_t ft81x_wait_for_queue_empty(ft81x *dev, TickType_t max_delay);
// esp_err_t ft81x_wait_for_queue_available(ft81x *dev, uint16_t len); // TODO
esp_err_t ft81x_bulk_cmd(ft81x *dev, uint32_t *p, uint16_t len);

size_t CMD_DL(uint32_t *p, uint32_t dl);
size_t CMD_DLSTART(uint32_t *p);
size_t CMD_DLSWAP(uint32_t *p);
size_t CMD_GAUGE(uint32_t *p, int16_t x, int16_t y, int16_t r, uint16_t options,
		 uint16_t major, uint16_t minor, uint16_t val, uint16_t range);
size_t CMD_FGCOLOR(uint32_t *p, uint32_t c);
size_t CMD_BGCOLOR(uint32_t *p, uint32_t c);
size_t CMD_GRADCOLOR(uint32_t *p, uint32_t c);
size_t CMD_NUMBER(uint32_t *p, int16_t x, int16_t y, int16_t font,
		  uint16_t options, int32_t n);
size_t CMD_TEXT(uint32_t *p, int16_t x, int16_t y, int16_t font,
		uint16_t options, char *s);
size_t CMD_ROMFONT(uint32_t *p, uint32_t font, uint32_t romslot);
size_t CMD_BUTTON(uint32_t *p, int16_t x, int16_t y, int16_t w, int16_t h,
		  int16_t font, uint16_t options, char *s);
size_t CMD_SNAPSHOT2(uint32_t *p, uint32_t fmt, uint32_t ptr, int16_t x,
		     int16_t y, int16_t w, int16_t h);
size_t CMD_SETBITMAP(uint32_t *p, uint32_t addr, uint16_t fmt, uint16_t width,
		     uint16_t height);
size_t CMD_SPINNER(uint32_t *p, int16_t x, int16_t y, uint16_t style,
		   uint16_t scale);
size_t CMD_CALIBRATE(uint32_t *p);
size_t CMD_SETROTATE(uint32_t *p, uint32_t r);
size_t CMD_MEMWRITE(uint32_t *p, reg_t addr, uint32_t val);

#endif
