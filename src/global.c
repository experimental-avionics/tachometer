#include "global.h"
#include "ft81x.h"

fram_header_v1_t fram_header = {0};
fram_data_v1_t fram_data = {0}; // Zero out the array at boot.
calc_data_v1_t calc_data = {0};
io_data_v1_t io_data = {0,0,0,128,13.8,-5.3,9.5};
tach_data_v1_t tach_data = {0};
fram_statistics_v1_t fram_statistics = {0};
fram_module_info_v1_t fram_info = {0};

static SemaphoreHandle_t lck_spi;
static SemaphoreHandle_t lck_fram_data;
static SemaphoreHandle_t lck_fram_statistics;
static SemaphoreHandle_t lck_fram_info;
static SemaphoreHandle_t lck_calc_data;
static SemaphoreHandle_t lck_io_data;
static SemaphoreHandle_t lck_tach_data;

esp_err_t state_init()
{
    // lock = xSemaphoreCreateBinary();
    // if (lock == NULL) return ESP_ERR_NO_MEM;
    // xSemaphoreGive(lock);

    lck_spi = xSemaphoreCreateBinary();
    if (lck_spi == NULL) return ESP_ERR_NO_MEM;
    xSemaphoreGive(lck_spi);

    lck_fram_data = xSemaphoreCreateBinary();
    if (lck_fram_data == NULL) return ESP_ERR_NO_MEM;
    xSemaphoreGive(lck_fram_data);

    lck_fram_statistics = xSemaphoreCreateBinary();
    if (lck_fram_statistics == NULL) return ESP_ERR_NO_MEM;
    xSemaphoreGive(lck_fram_statistics);

    lck_fram_info = xSemaphoreCreateBinary();
    if (lck_fram_info == NULL) return ESP_ERR_NO_MEM;
    xSemaphoreGive(lck_fram_info);

    lck_calc_data = xSemaphoreCreateBinary();
    if (lck_calc_data == NULL) return ESP_ERR_NO_MEM;
    xSemaphoreGive(lck_calc_data);

    lck_io_data = xSemaphoreCreateBinary();
    if (lck_io_data == NULL) return ESP_ERR_NO_MEM;
    xSemaphoreGive(lck_io_data);

    lck_tach_data = xSemaphoreCreateBinary();
    if (lck_tach_data == NULL) return ESP_ERR_NO_MEM;
    xSemaphoreGive(lck_tach_data);

	return ESP_OK;
}

inline esp_err_t lock_spi()
{
    return xSemaphoreTake(lck_spi, portMAX_DELAY) == pdTRUE ? ESP_OK : ESP_ERR_TIMEOUT;
}

inline esp_err_t unlock_spi()
{
    return xSemaphoreGive(lck_spi);
}

inline esp_err_t lock_fram_data() {
    return xSemaphoreTake(lck_fram_data, portMAX_DELAY) == pdTRUE ? ESP_OK : ESP_ERR_TIMEOUT;
}

inline esp_err_t unlock_fram_data() {
    return xSemaphoreGive(lck_fram_data);
}

inline esp_err_t lock_fram_statistics() {
    return xSemaphoreTake(lck_fram_statistics, portMAX_DELAY) == pdTRUE ? ESP_OK : ESP_ERR_TIMEOUT;
}

inline esp_err_t unlock_fram_statistics() {
    return xSemaphoreGive(lck_fram_statistics);
}

inline esp_err_t lock_fram_info() {
    return xSemaphoreTake(lck_fram_info, portMAX_DELAY) == pdTRUE ? ESP_OK : ESP_ERR_TIMEOUT;
    // return ESP_OK;
}

inline esp_err_t unlock_fram_info() {
    return xSemaphoreGive(lck_fram_info);
    // return ESP_OK;
}

inline esp_err_t lock_calc_data()
{
    return xSemaphoreTake(lck_calc_data, portMAX_DELAY) == pdTRUE ? ESP_OK : ESP_ERR_TIMEOUT;
}

inline esp_err_t unlock_calc_data()
{
    return xSemaphoreGive(lck_calc_data);
}

inline esp_err_t lock_io_data()
{
    return xSemaphoreTake(lck_io_data, portMAX_DELAY) == pdTRUE ? ESP_OK : ESP_ERR_TIMEOUT;
    // return ESP_OK;
}

inline esp_err_t unlock_io_data()
{
    return xSemaphoreGive(lck_io_data);
    // return ESP_OK;
}

inline esp_err_t lock_tach_data()
{
    return xSemaphoreTake(lck_tach_data, portMAX_DELAY) == pdTRUE ? ESP_OK : ESP_ERR_TIMEOUT;
}

inline esp_err_t unlock_tach_data()
{
    return xSemaphoreGive(lck_tach_data);
}

inline long map(long x, long in_min, long in_max, long out_min, long out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

inline long max(long a, long b)
{
	return (a > b) ? a : b;
}

inline long min(long a, long b)
{
	return (a < b) ? a : b;
}

inline uint32_t * ScaledGray(uint32_t * p, uint8_t brightness) 
{
    uint16_t shade = 100;
    if (brightness < 40) {
        shade += ((40 - brightness) * 4);
        if (shade > 255) shade = 255;
    }
    // ESP_LOGD("ScaledGray", "shade=%u brightness=%u", shade, brightness);
	p += CMD_DL(p, COLOR_RGB(shade, shade, shade));
    return p;
}
