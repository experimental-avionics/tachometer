#ifndef state_h
#define state_h

#pragma pack(push,1)
// Ref: https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/system/log.html
// ESP_LOG_NONE,       /*!< No log output */
// ESP_LOG_ERROR,      /*!< Critical errors, software module can not recover on its own */
// ESP_LOG_WARN,       /*!< Error conditions from which recovery measures have been taken */
// ESP_LOG_INFO,       /*!< Information messages which describe normal flow of events */
// ESP_LOG_DEBUG,      /*!< Extra information which is not necessary for normal use (values, pointers, sizes, etc). */
// ESP_LOG_VERBOSE     /*!< Bigger chunks of debugging information, or frequent messages which can potentially flood the output. */
#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
// Use the following calls
// ESP_LOGE - error (lowest)
// ESP_LOGW - warning
// ESP_LOGI - info
// ESP_LOGD - debug
// ESP_LOGV - verbose (highest)

#include <stdint.h>
#include <math.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <esp_log.h>

// Pin setup
#define BLINK_GPIO GPIO_NUM_2
#define MISO_GPIO GPIO_NUM_19
#define MOSI_GPIO GPIO_NUM_23
#define SCLK_GPIO GPIO_NUM_18
#define FT81X_CS GPIO_NUM_22
#define FT81X_RST GPIO_NUM_21
#define FRAM_CS GPIO_NUM_17
#define TACH_SIGNAL GPIO_NUM_16
#define CAN_TX GPIO_NUM_5
#define CAN_RX GPIO_NUM_35
#define BRIGHTNESS_INPUT ADC1_GPIO32_CHANNEL
#define VREF_PIN GPIO_NUM_25
#define BOOTLOAD_PIN GPIO_NUM_0

#define VOLTAGE_INPUT ADC1_GPIO39_CHANNEL
#define ALT_CURRENT_INPUT ADC1_GPIO33_CHANNEL
#define BAT_CURRENT_INPUT ADC1_GPIO34_CHANNEL

esp_err_t state_init();

esp_err_t lock_spi();
esp_err_t unlock_spi();
esp_err_t lock_fram_data();
esp_err_t unlock_fram_data();
esp_err_t lock_fram_statistics();
esp_err_t unlock_fram_statistics();
esp_err_t lock_fram_info();
esp_err_t unlock_fram_info();
esp_err_t lock_calc_data();
esp_err_t unlock_calc_data();
esp_err_t lock_io_data();
esp_err_t unlock_io_data();
esp_err_t lock_tach_data();
esp_err_t unlock_tach_data();

long map(long x, long in_min, long in_max, long out_min, long out_max);
long max(long a, long b);
long min(long a, long b);

#define MAX_SCREENS 200

typedef enum {
	smoothing_off,
	smoothing_low,
	smoothing_normal,
	smoothing_high,
} rpm_smoothing_t;

typedef struct fram_header_v1_s {
	uint32_t majorversion;		// Version of this structure of ram
	uint32_t minorversion;		// Version of this structure of ram
	uint32_t spare;				// Empty space
} fram_header_v1_t;

typedef struct fram_data_v1_s {
	uint32_t crc;				// calculated CRC
    uint64_t hobbs_time;		// Microseconds
    uint64_t tach_time;			// Microseconds
    uint32_t transform_a;		// Screen params
    uint32_t transform_b;		// Screen params
    uint32_t transform_c;		// Screen params
    uint32_t transform_d;		// Screen params
    uint32_t transform_e;		// Screen params
    uint32_t transform_f;		// Screen params
	int32_t  green_arc_low;		// RPM
	int32_t  green_arc_high;	// RPM
	int32_t  red_line;			// RPM
	int32_t  cruise_rpm;		// RPM
	int32_t  rpm_max_scale;		// RPM
	int32_t  bat_amps;			// Amps
	int32_t  bat_mvolts;		// mVolts
	int32_t  alt_amps;			// Amps
	int32_t  alt_mvolts;		// mVolts
	int32_t  volts_low;			// mVolts
	int32_t  volts_high;		// mVolts
	int16_t  max_rpm_drop;		// RPM
	int8_t   bat_reverse;		// reverse = 1, not = 0
	int8_t   alt_reverse;		// reverse = 1, not = 0
	int32_t  rpm_smoothing;		// 1,2,3 = low,normal,high
	int32_t  pulses_per_rev;	// number of positive pulses per revolution
	uint32_t min_bright_adc;	// 
	uint32_t max_bright_adc;	// 
} fram_data_v1_t;

typedef struct fram_statistics_v1_s {
	uint32_t crc;				// calculated CRC
	uint64_t total_time;		// Microseconds
	uint32_t max_volts;			// mVolts
	uint32_t max_rpm;			// RPM
	uint64_t volts_histogram[48]; // TBD
} fram_statistics_v1_t;

typedef struct fram_module_info_v1_s {
	uint32_t crc;				// calculated CRC
	uint64_t module_type;		// module type
	uint32_t module_version;	// version # of this module
	uint32_t module_serial;		// serial number
} fram_module_info_v1_t;

typedef struct calc_data_v1_s {
	double green_low_angle;
	double green_high_angle;
	double green_step_size;
	uint32_t green_high_x;
	uint32_t green_high_y;
	
	double red_line_angle;
	uint32_t redline_x1;
	uint32_t redline_y1;
	uint32_t redline_x2;
	uint32_t redline_y2;

	int major_rpm_step_size;
	uint64_t tach_time_increment;
} calc_data_v1_t;

#define TACH_MODE_TROUBLESHOOT 1
#define TACH_MODE_NORMAL 0
typedef struct io_data_v1_s {
	uint32_t rpm_hold;		// RPM
	uint32_t rpm_avg;		// RPM
	uint64_t flight;		// microseconds
	uint8_t brightness;		// 0-128
	float volts;			// mVolts
	float bat_amps;			// mAmps
	float alt_amps;			// mAmps
	uint32_t tach_mode; 	// 0 = normal, 1 = troubleshoot mode
} io_data_v1_t;

#define MAX_TACH_SAMPLES 10
typedef struct tach_data_v1_s {
	int32_t tach_index;
	uint32_t tach_state[MAX_TACH_SAMPLES];
	uint32_t tach_delta[MAX_TACH_SAMPLES];
} tach_data_v1_t;

extern fram_header_v1_t fram_header;
extern fram_data_v1_t fram_data;
extern calc_data_v1_t calc_data;
extern io_data_v1_t io_data;
extern tach_data_v1_t tach_data;
extern fram_statistics_v1_t fram_statistics;
extern fram_module_info_v1_t fram_info;

uint32_t * ScaledGray(uint32_t *p, uint8_t brightness);

#pragma pack(pop)

#endif