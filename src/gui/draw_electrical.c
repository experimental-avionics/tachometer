#include "gui.h"

static const uint16_t graph_left  = 100;
static const uint16_t graph_right = 550;

inline static uint32_t * draw_info_bar(uint32_t * p, uint8_t brightness, uint32_t y, char * title, 
			bool enabled, float val, char * fmt_st,
			float min_val, float low_red, float low_yellow, 
			float hi_green, float hi_yellow, float hi_red, float max_val)
			// If any of the params are not used, set it to lower than min_graph
{
	// ESP_LOGD("pix", "input %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f", val,min_val,low_red,low_yellow,hi_green,hi_yellow,hi_red,max_val);

	float val_per_pix = (float)(graph_right - graph_left) / (max_val - min_val);
	uint32_t low_pix = graph_left * 16;

	if (enabled) {
		p += CMD_DL(p, BEGIN(RECTS));

		// Make the bar 17 pixels wide
		uint32_t top_y = (y-8) * 16;
		uint32_t bot_y = (y+8) * 16;

		// Make the colored regions
		#define MAKE_REGION(a,b) if (min_val < a) {							\
			p += CMD_DL(p, b);												\
			p += CMD_DL(p, VERTEX2F(low_pix, top_y));						\
			low_pix = (graph_left + ((a - min_val) * val_per_pix)) * 16;	\
			p += CMD_DL(p, VERTEX2F(low_pix, bot_y)); }
		MAKE_REGION(low_red,COLOR_RED);
		MAKE_REGION(low_yellow,COLOR_YELLOW);
		MAKE_REGION(hi_green,COLOR_GREEN);
		MAKE_REGION(hi_yellow,COLOR_YELLOW);
		MAKE_REGION(hi_red,COLOR_RED);

		// Make the vertical line for the current value
		p += CMD_DL(p, BEGIN(LINES));
		p += CMD_DL(p, COLOR_WHITE);
		low_pix = (graph_left + ((val - min_val) * val_per_pix)) * 16;
		if (low_pix < (graph_left*16)) low_pix = (graph_left*16);
		if (low_pix > (graph_right*16)) low_pix = (graph_right*16);
		p += CMD_DL(p, LINE_WIDTH(1 * 16));
		p += CMD_DL(p, VERTEX2F(low_pix, (y-15) * 16));
		p += CMD_DL(p, VERTEX2F(low_pix, (y+15) * 16));
		p += CMD_DL(p, LINE_WIDTH(4 * 16));
		p += CMD_DL(p, VERTEX2F(low_pix, (y-20) * 16));
		p += CMD_DL(p, VERTEX2F(low_pix, (y-13) * 16));
		p += CMD_DL(p, VERTEX2F(low_pix, (y+13) * 16));
		p += CMD_DL(p, VERTEX2F(low_pix, (y+20) * 16));

		// If the value is in a yellow or red zone, put a background behind the number
		float base = min_val;
		uint32_t backcolor = 0;

		#define SET_BACK(a,b) if (min_val < a) { if (base <= val && val < a && backcolor == 0) { backcolor = b; } base = a; }
		if (val < min_val) { backcolor = COLOR_RED; }
		SET_BACK(low_red,COLOR_RED);
		SET_BACK(low_yellow,COLOR_YELLOW);
		SET_BACK(hi_green,0);
		SET_BACK(hi_yellow,COLOR_YELLOW);
		SET_BACK(hi_red,COLOR_RED);
		if (val > max_val) { backcolor = COLOR_RED; }
		if (backcolor != 0) {
			p += CMD_DL(p, BEGIN(RECTS));
			p += CMD_DL(p, backcolor);
			p += CMD_DL(p, VERTEX2F((graph_right + 20) * 16, (y - 30) * 16));
			p += CMD_DL(p, VERTEX2F((             700) * 16, (y + 30) * 16));
		}

		// Display the value on the right side
		char _buf[16];
		snprintf(_buf, sizeof(_buf), fmt_st, val);
		p += CMD_DL(p, COLOR_WHITE);
		p += CMD_TEXT(p, 700, y, 2, OPT_CENTERY+OPT_RIGHTX, _buf);

	} else { // NOT ENABLED
		// Put red X over everything
		p += CMD_DL(p, LINE_WIDTH(5*16));
		p += CMD_DL(p, COLOR_RED);
		p += CMD_DL(p, BEGIN(LINES));
		p += CMD_DL(p, VERTEX2F(graph_left  * 16, (y + 30) * 16));
		p += CMD_DL(p, VERTEX2F(graph_right * 16, (y - 30) * 16));
		p += CMD_DL(p, VERTEX2F(graph_left  * 16, (y - 30) * 16));
		p += CMD_DL(p, VERTEX2F(graph_right * 16, (y + 30) * 16));
		p += CMD_DL(p, END());
	}

	p = ScaledGray(p, brightness);
	p += CMD_TEXT(p, (graph_left+graph_right)/2, y-40, 30, OPT_CENTER, title);

	return p;
}

esp_err_t gui_draw_electrical(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	// if (!force_refresh) return ESP_OK;
	// lock_io_data(); // take and give must be around all global variables
	// typeof(io_data.volts) _volts = io_data.volts;
	// typeof(io_data.bat_amps) _bat_amps = io_data.bat_amps;
	// typeof(io_data.alt_amps) _alt_amps = io_data.alt_amps;
	// unlock_io_data();

	// FOR DEBUGGING
		static float _volts = 0;
		static float _bat_amps = 0;
		static float _alt_amps = 0;

		static float step_volts = 0.01;
		static float step_bat_amps = 0.05;
		static float step_alt_amps = 0.03;

		_volts += step_volts;
		if (_volts >= 20.0 || _volts <= -2.0) step_volts = -step_volts;
		_bat_amps += step_bat_amps;
		if (_bat_amps >= 52.0 || _bat_amps <= -22.0) step_bat_amps = -step_bat_amps;
		_alt_amps += step_alt_amps;
		if (_alt_amps >= 52.0 || _alt_amps <= -2.0) step_alt_amps = -step_alt_amps;
	// END DEBUGGING

	INIT_DISPLAY_BUFFER

	p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

	p = draw_info_bar(p, brightness, 125,"VOLTAGE",true,_volts,"%0.1f",0.0,9.0,11.0,14.0,15.0,18.0,18.0);
	p = draw_info_bar(p, brightness, 250,"BATTERY AMPS",true,_bat_amps,"%0.1f",-20.0,-99.0,-10.0,35.0,45.0,50.0,50.0);
	p = draw_info_bar(p, brightness, 375,"ALTERNATOR AMPS",false,_alt_amps,"%0.1f",0.0,-99.0,-99.0,35.0,45.0,50.0,50.0);

	PAGE_TITLE("ELECTRICAL");
	p = ScaledGray(p, brightness);
	p += CMD_DL(p, LINE_WIDTH(3 * 16));
	if (e.is_alt) {
		p += CMD_DL(p, BEGIN(LINE_STRIP));
		p += CMD_DL(p, VERTEX2F(770 * 16, 200 * 16));
		p += CMD_DL(p, VERTEX2F(790 * 16, 240 * 16));
		p += CMD_DL(p, VERTEX2F(770 * 16, 280 * 16));
	}
	p += CMD_DL(p, BEGIN(LINE_STRIP));
	p += CMD_DL(p, VERTEX2F(30 * 16, 200 * 16));
	p += CMD_DL(p, VERTEX2F(10 * 16, 240 * 16));
	p += CMD_DL(p, VERTEX2F(30 * 16, 280 * 16));

	// Put red X over everything
	// p += CMD_DL(p, LINE_WIDTH(10*16));
	// p += CMD_DL(p, COLOR_RED);
	// p += CMD_DL(p, BEGIN(LINES));
	// p += CMD_DL(p, VERTEX2F(0, 0));
	// p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));
	// p += CMD_DL(p, VERTEX2F(0, 480 * 16));
	// p += CMD_DL(p, VERTEX2F(800 * 16, 0));
	// p += CMD_DL(p, END());

	p += CMD_DL(p, SAVE_CONTEXT());
	p += CMD_DL(p, LINE_WIDTH(1 * 16));
	p += CMD_DL(p, COLOR_WHITE);
	p += CMD_DL(p, COLOR_A(0));
	p += CMD_DL(p, BEGIN(RECTS));
	// Left arrow
	p += CMD_DL(p, TAG(PAGE_TACHOMETER));
	p += CMD_DL(p, VERTEX2F(0 * 16, 0 * 16));
	p += CMD_DL(p, VERTEX2F(100 * 16, 480 * 16));
	// Right arrow
	if (e.is_alt) {
		p += CMD_DL(p, TAG(PAGE_SETTINGS_ELEC));
		p += CMD_DL(p, VERTEX2F(700 * 16, 0 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));
	}
	p += CMD_DL(p, RESTORE_CONTEXT());

	// Dump buffer to display
	UPDATE_DISPLAY

	return ESP_OK;
}
