#include "gui.h"

esp_err_t gui_draw_settings_elec(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	// Just switching to this page, wait for button release
	if (e.tag == PAGE_SETTINGS_ELEC && e.is_down) {
		return ESP_OK;
	}

	// lockxx(); // take and give must be around all global variables
	// unlockxx();

	if (force_refresh) {
		// ESP_LOGD("gui", "gui_draw_settings_elec");

		INIT_DISPLAY_BUFFER

		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

		MAKE_2x4_BUTTON(PAGE_SET_BAT_AMPS_SCALE,  BUTTON_COL_1,BUTTON_ROW_1,"SET BAT AMPS MAX")
		MAKE_2x4_BUTTON(PAGE_SET_ALT_AMPS_SCALE,  BUTTON_COL_1,BUTTON_ROW_2,"SET ALT AMPS MAX")
		MAKE_2x4_BUTTON(PAGE_SET_VOLT_WARN_LOW,   BUTTON_COL_1,BUTTON_ROW_4,"SET LOW VOLT WARN")

		MAKE_2x4_BUTTON(PAGE_SET_BAT_MVOLTS_SCALE,BUTTON_COL_2,BUTTON_ROW_1,"SET BAT MV MAX")
		MAKE_2x4_BUTTON(PAGE_SET_ALT_MVOLTS_SCALE,BUTTON_COL_2,BUTTON_ROW_2,"SET ALT MV MAX")
		MAKE_2x4_BUTTON(PAGE_SET_VOLT_WARN_HIGH,  BUTTON_COL_2,BUTTON_ROW_4,"SET HIGH VOLT WARN")

		PAGE_TITLE("ELECTRICAL SETTINGS");

		DRAW_PAGE_LEFT(PAGE_ELECTRICAL);
		DRAW_PAGE_RIGHT(PAGE_SETTINGS_TACH);

		UPDATE_DISPLAY
	}

	return ESP_OK;
}
