#include "gui.h"

esp_err_t gui_draw_settings_info(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	// Just switching to this page, wait for button release
	if (e.tag == PAGE_SETTINGS_TACH && e.is_down) {
		return ESP_OK;
	}

	// lockxx(); // take and give must be around all global variables
	// unlockxx();

	if (e.state_changed && e.is_down) {
		switch (e.tag) {
			case MAX_SCREENS+ 0:
				lock_fram_data();
				fram_data.min_bright_adc = UINT32_MAX;
				fram_data.max_bright_adc = 0;
				unlock_fram_data();
				gui_current_fg = PAGE_SETTINGS_TACH;
				break; 
			// case MAX_SCREENS+ 1:	;	break;
		}
	}

	if (force_refresh) {
		// ESP_LOGD("gui", "gui_draw_settings_info");

		INIT_DISPLAY_BUFFER

		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

		MAKE_3x4_BUTTON(MAX_SCREENS+ 0,  100,BUTTON_ROW_1,"Reset","Brightness")
		MAKE_3x4_BUTTON(PAGE_NULL,  100,BUTTON_ROW_2,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  100,BUTTON_ROW_3,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  100,BUTTON_ROW_4,"X","X")

		MAKE_3x4_BUTTON(PAGE_NULL,  310,BUTTON_ROW_1,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  310,BUTTON_ROW_2,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  310,BUTTON_ROW_3,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  310,BUTTON_ROW_4,"X","X")
		
		MAKE_3x4_BUTTON(PAGE_NULL,  520,BUTTON_ROW_1,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  520,BUTTON_ROW_2,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  520,BUTTON_ROW_3,"X","X")
		MAKE_3x4_BUTTON(PAGE_NULL,  520,BUTTON_ROW_4,"X","X")

		PAGE_TITLE("INFORMATION SETTINGS");

		DRAW_PAGE_LEFT(PAGE_SETTINGS_TACH)

		UPDATE_DISPLAY
	}
	
	return ESP_OK;
}
