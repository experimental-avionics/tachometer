#include "gui.h"

esp_err_t gui_draw_splash(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	static uint32_t counter = 0;

	// lockxx(); // take and give must be around all global variables
	// unlockxx();

	if (!counter) {
		INIT_DISPLAY_BUFFER
		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

		// TODO: Display cool bitmap
		p += CMD_DL(p, COLOR_RED);
		p += CMD_TEXT(p, 400, 200, 2, OPT_CENTER, "COOL SPLASH SCREEN");
		p += CMD_DL(p, COLOR_BLUE);
		p += CMD_TEXT(p, 400, 300, 2, OPT_CENTER, "Experimental Avionics Inc.");

		UPDATE_DISPLAY
	}

	if (counter++ > 100) { // 400 = about 4 seconds
		gui_current_fg = PAGE_TACHOMETER;
	}

	return ESP_OK;
}
