#include "gui.h"

esp_err_t gui_draw_tach_background(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	static uint32_t bitmap_addr = 0;

	INIT_DISPLAY_BUFFER

	p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

	// Draw the green arc
	p += CMD_DL(p, LINE_WIDTH(7 * 16));
	p += CMD_DL(p, COLOR_GREEN);
	p += CMD_DL(p, BEGIN(LINE_STRIP));
	lock_calc_data();
	for (float i = calc_data.green_low_angle; i < calc_data.green_high_angle; i += calc_data.green_step_size) {
		uint32_t x = 240 + (182 * sin(i));
		uint32_t y = 240 - (182 * cos(i));
		p += CMD_DL(p, VERTEX2F(x * 16, y * 16));
	}
	// Sometimes float math means 1 + 1 != 2, so this ensures the line ends in the correct spot.
	p += CMD_DL(p, VERTEX2F(calc_data.green_high_x * 16, calc_data.green_high_y * 16));
	p += CMD_DL(p, END());

	// Draw gauge
	p += CMD_DL(p, COLOR_WHITE);
	p += CMD_GAUGE(p, 240, 240, 240, OPT_FLAT | OPT_NOPOINTER | OPT_NOBACK,
		       MAJOR_TICKS, MINOR_TICKS, 0, fram_data.rpm_max_scale);
	p = ScaledGray(p, brightness);
	p += CMD_TEXT(p, 240, 220, 30, OPT_CENTER, "RPM");
	p += CMD_TEXT(p, 240, 260, 29, OPT_CENTER, "x100");
	p += CMD_TEXT(p, 600, 75, 30, OPT_CENTER, "HOBBS");
	p += CMD_TEXT(p, 600, 200, 30, OPT_CENTER, "TACH");
	p += CMD_TEXT(p, 600, 325, 30, OPT_CENTER, "FLIGHT");
	lock_fram_data();
	for (int i = 0; i <= fram_data.rpm_max_scale; i += calc_data.major_rpm_step_size) {
		const int n = i / 100;
		const float angle =	(270.0f * i / fram_data.rpm_max_scale - 135.0f) * PI / 180.0f;
		const float radius = (n < 10) ? 140 : 130;
		uint32_t x = 240 + (radius * sin(angle));
		uint32_t y = 240 - (radius * cos(angle));
		p += CMD_NUMBER(p, x, y, 31, OPT_CENTER, n);
	}
	unlock_fram_data();

	// Page selector overlay
	PAGE_TITLE("TACHOMETER");

	// Draw engine red line mark
	p += CMD_DL(p, COLOR_RED);
	p += CMD_DL(p, LINE_WIDTH(120));
	p += CMD_DL(p, BEGIN(LINES));
	p += CMD_DL(p, VERTEX2F(calc_data.redline_x1 * 16, calc_data.redline_y1 * 16));
	p += CMD_DL(p, VERTEX2F(calc_data.redline_x2 * 16, calc_data.redline_y2 * 16));
	p += CMD_DL(p, END());
	unlock_calc_data();

	// Draw the background on the screen //////////////////////////////////////
	UPDATE_DISPLAY

	// Save background image //////////////////////////////////////////////////
	p = _p;
	p += CMD_SNAPSHOT2(p, RGB565, bitmap_addr, 0, 0, 800, 480);
	lock_spi();
	ft81x_bulk_cmd(_disp, _p, p - _p);
	ft81x_wait_for_queue_empty(_disp, 500 / portTICK_PERIOD_MS);
	unlock_spi();

	return ESP_OK;
}
