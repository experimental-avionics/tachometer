#include "gui.h"

esp_err_t gui_draw_tach_foreground(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
#define DELTA_LOC_X 233
#define DELTA_LOC_Y 320
	static uint8_t delta_mode = 0;
	static uint32_t _delta_base_rpm = 0.f;

	static uint32_t bitmap_addr = 0;

	lock_fram_data(); // take and give must be around all global variables
	typeof(fram_data.hobbs_time) _hobbs = fram_data.hobbs_time;
	typeof(fram_data.tach_time) _tach = fram_data.tach_time;
	uint32_t _max_rpm_drop = fram_data.max_rpm_drop;
	unlock_fram_data();
	lock_io_data();
	typeof(io_data.rpm_avg) _rpm_avg = io_data.rpm_avg;
	typeof(io_data.rpm_hold) _rpm_hold = io_data.rpm_hold;
	typeof(io_data.flight) _flight = io_data.flight;
	unlock_io_data();

	if (e.state_changed && e.is_down && e.tag == 251) {
		if (delta_mode) {
			delta_mode = 0;
		} else {
			delta_mode = 1;
			_delta_base_rpm = _rpm_hold;
		}
	}

	char hobbs_buf[16], tach_buf[16], time_buf[16];
	snprintf(hobbs_buf, sizeof(hobbs_buf), "%0.2f",
			(double)(_hobbs * 100 / 1000000 / 3600) / 100.0);
	snprintf(tach_buf, sizeof(tach_buf), "%0.2f",
			(double)(_tach * 100 / 1000000 / 3600) / 100.0);
	snprintf(time_buf, sizeof(time_buf), "%02llu:%02llu:%02llu",
			_flight / 1000000 / 3600, _flight / 1000000 / 60 % 60,
			_flight / 1000000 % 60);

	INIT_DISPLAY_BUFFER

	p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

	// Load static background bitmap
	p += CMD_SETBITMAP(p, bitmap_addr, RGB565, 800, 480);
	p += CMD_DL(p, BEGIN(BITMAPS));
	p += CMD_DL(p, VERTEX2F(0, 0));

	// Draw times and clock
	p += CMD_DL(p, COLOR_WHITE);
	p += CMD_TEXT(p, 600, 125, 2, OPT_CENTER, hobbs_buf);
	p += CMD_TEXT(p, 600, 250, 2, OPT_CENTER, tach_buf);
	p += CMD_TEXT(p, 600, 375, 2, OPT_CENTER, time_buf);

	// If the engine is past red line, display a red box behind the readout
	if (_rpm_avg > fram_data.red_line) {
		p += CMD_DL(p, SAVE_CONTEXT());
		p += CMD_DL(p, COLOR_RED);
		p += CMD_DL(p, BEGIN(RECTS));
		p += CMD_DL(p, VERTEX2F(150 * 16, 360 * 16));
		p += CMD_DL(p, VERTEX2F(330 * 16, 440 * 16));
		p += CMD_DL(p, RESTORE_CONTEXT());
	} 

	// Show RPM indications
	if (delta_mode == 1) {
		int32_t delta = (int32_t)_rpm_hold-(int32_t)_delta_base_rpm;
		p += CMD_DL(p, SAVE_CONTEXT());
		if (delta < -_max_rpm_drop) p += CMD_DL(p, COLOR_YELLOW);
		else p += CMD_DL(p, COLOR_BLUE);
		p += CMD_DL(p, BEGIN(RECTS)); 
		p += CMD_DL(p, VERTEX2F((DELTA_LOC_X-70) * 16, (DELTA_LOC_Y-30) * 16));
		p += CMD_DL(p, VERTEX2F((DELTA_LOC_X+70) * 16, (DELTA_LOC_Y+30) * 16));
		p += CMD_DL(p, RESTORE_CONTEXT());
		char buf[16];
		snprintf(buf, sizeof(buf), "%d",delta);
		p += CMD_TEXT(p, DELTA_LOC_X, DELTA_LOC_Y, 2, OPT_CENTER, buf);
	}
 	p += CMD_NUMBER(p, 240, 400, 1, OPT_CENTER, _rpm_hold);
	// Make the needle red if it is past the red line
	if (_rpm_avg > fram_data.red_line) p += CMD_DL(p, COLOR_RED);
	p += CMD_GAUGE(p, 240, 240, 240,
				OPT_FLAT | OPT_NOBACK | OPT_NOTICKS, 7, 5,
				(uint16_t)_rpm_avg, fram_data.rpm_max_scale);

	// Volts and Amps
	#define A_X 565
	#define B_X 600
	#define V_X 635
	#define V_Y 20
	#define V_Delta 15
	p += CMD_DL(p, BEGIN(RECTS));
	p += CMD_DL(p, COLOR_GREEN);
	p += CMD_DL(p, VERTEX2F((A_X-V_Delta) * 16, (V_Y-V_Delta) * 16));
	p += CMD_DL(p, VERTEX2F((A_X+V_Delta) * 16, (V_Y+V_Delta) * 16));
	p += CMD_DL(p, VERTEX2F((B_X-V_Delta) * 16, (V_Y-V_Delta) * 16));
	p += CMD_DL(p, VERTEX2F((B_X+V_Delta) * 16, (V_Y+V_Delta) * 16));
	p += CMD_DL(p, VERTEX2F((V_X-V_Delta) * 16, (V_Y-V_Delta) * 16));
	p += CMD_DL(p, VERTEX2F((V_X+V_Delta) * 16, (V_Y+V_Delta) * 16));
	p += CMD_DL(p, COLOR_WHITE);
	p += CMD_TEXT(p, A_X, V_Y, 29, OPT_CENTER, "A");
	p += CMD_TEXT(p, B_X, V_Y, 29, OPT_CENTER, "B");
	p += CMD_TEXT(p, V_X, V_Y, 29, OPT_CENTER, "V");

	p = ScaledGray(p, brightness);
	p += CMD_DL(p, LINE_WIDTH(3 * 16));
	if (e.is_alt) {
		// Draw left arrow
		p += CMD_DL(p, BEGIN(LINE_STRIP));
		p += CMD_DL(p, VERTEX2F(30 * 16, 200 * 16));
		p += CMD_DL(p, VERTEX2F(10 * 16, 240 * 16));
		p += CMD_DL(p, VERTEX2F(30 * 16, 280 * 16));
	}
	// Draw right arrow
	p += CMD_DL(p, BEGIN(LINE_STRIP));
	p += CMD_DL(p, VERTEX2F(770 * 16, 200 * 16));
	p += CMD_DL(p, VERTEX2F(790 * 16, 240 * 16));
	p += CMD_DL(p, VERTEX2F(770 * 16, 280 * 16));

	p += CMD_DL(p, SAVE_CONTEXT());
	p += CMD_DL(p, LINE_WIDTH(1 * 16));
	p += CMD_DL(p, COLOR_GREEN);
	p += CMD_DL(p, COLOR_A(0));
	p += CMD_DL(p, BEGIN(RECTS));
	if (e.is_alt) {
		// Left arrow tag
		p += CMD_DL(p, TAG(PAGE_SETTINGS_TACH));
		p += CMD_DL(p, VERTEX2F(  0 * 16,   0 * 16));
		p += CMD_DL(p, VERTEX2F(100 * 16, 480 * 16));
	}
	// Right arrow tag
	p += CMD_DL(p, TAG(PAGE_ELECTRICAL));
	p += CMD_DL(p, VERTEX2F(700 * 16,   0 * 16));
	p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));

	if (!e.is_alt) {				// Normal operation
		p += CMD_DL(p, TAG(251));	// Tag for RPM Delta
		p += CMD_DL(p, VERTEX2F(  0 * 16,   0 * 16)); // Most of the screen
		p += CMD_DL(p, VERTEX2F(690 * 16, 480 * 16));
	}
	p += CMD_DL(p, RESTORE_CONTEXT());

	UPDATE_DISPLAY

	return ESP_OK;
}
