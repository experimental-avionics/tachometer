#include "global.h"
#include "fram.h"
#include "framdata.h"
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>

// These functions manage and maintain the data in FRAM
static fram nvram;

static fram_header_v1_t old_header;
static fram_data_v1_t old_data;
static fram_statistics_v1_t old_statistics;
static fram_module_info_v1_t old_info;

void dump_fram()
{
	ESP_LOGD("fram", "dump_fram+");
	union {
		uint32_t d32;
		uint8_t d8[4];
	} d;
	char ln[10]  = {0};
	char st[150] = {0};
	char * pt = st;
	int i,j;
	lock_spi();
	for (i = 0,j = 0; i < 2048; i+=4) {
		fram_read(&nvram, i, &d, sizeof(d));
		sprintf(pt,"%08x %c",d.d32,0);
		pt += 9;
		if (++j > 7 || i+4 >= 2048) {
			j = 0;
			sprintf(ln,"%5d",i-28); // 28=32-4
			ESP_LOGD(ln, "%s", st);
			pt = st;
			st[0] = 0;
		}
	}
	unlock_spi();
	ESP_LOGD("fram", "dump_fram-");
}

uint32_t MakeCRC(const char *buf, size_t len) {
	// https://rosettacode.org/wiki/CRC-32
	static uint32_t table[256];
	static int have_table = 0;
	uint32_t rem;
	uint8_t octet;
	int i, j;
	const char *p, *q;
 
	// First pass, calculate the CRC table and keep it around.
	if (have_table == 0) {
		// Calculate CRC table.
		for (i = 0; i < 256; i++) {
			rem = i;  // remainder from polynomial division
			for (j = 0; j < 8; j++) {
				if (rem & 1) {
					rem >>= 1;
					rem ^= 0xedb88320;
				} else
					rem >>= 1;
			}
			table[i] = rem;
		}
		have_table = 1;
	}
 
	uint32_t crc = ~0;
	q = buf + len;
	for (p = buf; p < q; p++) {
		octet = *p;  // Cast to unsigned octet.
		crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
	}
	return ~crc;
}

inline uint64_t make_version(uint32_t major, uint32_t minor) 
{
    uint64_t rtn = ((uint64_t)major) << 32 | minor;
	return rtn;
}

void upgrade_fram_data_to_1_1(uint64_t current_version)
{
    lock_fram_data();
    fram_data.hobbs_time		= 1;			// 1 microsecond of time
    fram_data.tach_time			= 1;			// 1 microsecond of time

    fram_data.green_arc_low		= 2000;			// RPM
    fram_data.green_arc_high	= 2700;			// RPM
    fram_data.red_line			= 2700;			// RPM
    fram_data.cruise_rpm		= 2550;			// RPM
    fram_data.rpm_max_scale		= 3500;			// RPM
    fram_data.bat_amps			= 50;			// Amps
    fram_data.bat_mvolts		= 50;			// mVolts
    fram_data.alt_amps			= 50;			// Amps
    fram_data.alt_mvolts		= 50;			// mVolts
    fram_data.volts_low			= 9.99;			// mVolts
    fram_data.volts_high		= 16.0;			// mVolts
    fram_data.max_rpm_drop		= 75;			// RPM
    fram_data.rpm_smoothing 	= smoothing_normal; // 
    fram_data.pulses_per_rev 	= 3;			// 3 pulses per rev for debugging TODO:
    unlock_fram_data();
}

void setup_feram()
{
	fram_init(&nvram, HSPI_HOST, FRAM_CS);

	// dump_f ram();
	// Load all the memory from the FRAM
	ESP_LOGD("fram", "Loading config from FRAM");
	lock_fram_data();
	lock_fram_info();
	lock_fram_statistics();
	lock_spi();
	fram_read(&nvram,   0, &fram_header, 		sizeof(fram_header));
	fram_read(&nvram,  12, &fram_data, 			sizeof(fram_data));
	fram_read(&nvram,1000, &fram_statistics, 	sizeof(fram_statistics));
	fram_read(&nvram,2000, &fram_info, 			sizeof(fram_info));
	unlock_spi();
    // Save off the starting point.
    old_header = fram_header;
    old_data = fram_data;
    old_statistics = fram_statistics;
    old_info = fram_info;
	unlock_fram_data();
	unlock_fram_info();
	unlock_fram_statistics();

	uint64_t thisversion = make_version(fram_header.majorversion, fram_header.minorversion);

	if (thisversion != make_version(1,1)) {
        
        upgrade_fram_data_to_1_1(thisversion);

		lock_fram_statistics();
		ESP_LOGD("fram", "### OLD FRAM version: %u.%u", fram_header.majorversion, fram_header.minorversion);
		unlock_fram_statistics();

		fram_header.majorversion	= 1;			// version 1
		fram_header.minorversion	= 1;			// version 1
		fram_header.spare 			= 0x656E6479;	// spare
		
		lock_fram_info();
		fram_info.module_type 	    = 0x54616368;	// ASCII "Tach"
		fram_info.module_version 	= 1;			// version 1
		fram_info.module_serial 	= 1992;			// SN 1992
		unlock_fram_info();
	}

    // If something is different, save the new stuff
    if (memcmp(&old_header,&fram_header,sizeof(fram_header))) {
        // No CRC to create
        // No locks on this structure
        old_header = fram_header;
        // Write the old structure to FRAM
        lock_spi();
        fram_write(&nvram, 0, &fram_header, sizeof(fram_header));
        unlock_spi();
    }

	// Data sanity checks
	if (fram_data.max_bright_adc <= fram_data.min_bright_adc) { fram_data.max_bright_adc = 0;}
	if (fram_data.min_bright_adc == 0) {fram_data.min_bright_adc = 0xFFFFFFFE;}

	if (fram_data.rpm_max_scale == 0 || (fram_data.rpm_max_scale != 3500 && fram_data.rpm_max_scale != 7000)) {
			fram_data.rpm_max_scale = 3500;}	// RPM
	if (fram_data.green_arc_low < 500 || fram_data.green_arc_low > fram_data.rpm_max_scale) {
			fram_data.green_arc_low = 2000;}	// RPM
	if (fram_data.green_arc_high < 500 || fram_data.green_arc_high > fram_data.rpm_max_scale) {
			fram_data.green_arc_high = 2700;}	// RPM
	if (fram_data.red_line < 500 || fram_data.red_line > fram_data.rpm_max_scale) {
			fram_data.red_line = 2700;}	// RPM
	if (fram_data.cruise_rpm < 500 || fram_data.cruise_rpm > fram_data.rpm_max_scale) {
			fram_data.cruise_rpm = 2550;}	// RPM

	if (fram_data.bat_amps == 0) {fram_data.bat_amps = 50;}	// Amps
	if (fram_data.bat_mvolts == 0) {fram_data.bat_mvolts = 50;}	// mVolts
	if (fram_data.alt_amps == 0) {fram_data.alt_amps = 50;}	// Amps
	if (fram_data.alt_mvolts == 0) {fram_data.alt_mvolts = 50;}	// mVolts
	if (fram_data.volts_low == 0) {fram_data.volts_low = 9.99;}	// mVolts
	if (fram_data.volts_high == 0) {fram_data.volts_high = 16.0;}	// mVolts
	if (fram_data.max_rpm_drop == 0) {fram_data.max_rpm_drop = 75;}	// RPM
	if (fram_data.rpm_smoothing == 0) {fram_data.rpm_smoothing = smoothing_normal;}
	if (fram_data.pulses_per_rev == 0) {fram_data.pulses_per_rev = 1;}	// 3 pulses per rev for debugging TODO:
	if (fram_info.module_type == 0) {fram_info.module_type = 0x54616368;}	// ASCII "Tach"
	if (fram_info.module_version == 0) {fram_info.module_version = 1;}		// version 1
	if (fram_info.module_serial == 0) {fram_info.module_serial = 1992;}	// SN 1992

	// lock_fram_data();
	// ESP_LOGD("fram", "FRAM version: %u", fram_header.version);
	// ESP_LOGD("fram", "Hobbs time set to %llu.%06llu from FRAM", fram_data.hobbs_time / 1000000, fram_data.hobbs_time % 1000000);
	// ESP_LOGD("fram", "Tach time set to %llu.%06llu from FRAM", fram_data.tach_time / 1000000, fram_data.tach_time % 1000000);
	// ESP_LOGD("FRAM", "Display Calibration Read 0x%04X %04X %04X %04X %04X %04X", 
	// 	fram_data.transform_a,fram_data.transform_b,fram_data.transform_c,
	// 	fram_data.transform_d,fram_data.transform_e,fram_data.transform_f);
	// unlock_fram_data();
}

void fram_task(void *pvParameter)
{
	ESP_LOGD("FRAM", "Starting fram_task thread");

	gpio_pad_select_gpio(BLINK_GPIO);
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
	// uint32_t i = 0;
	while (1) {
        // If something is different, save the new stuff
        if (memcmp(&old_data,&fram_data,sizeof(fram_data))) {
            ESP_LOGD("FRAM","Saved fram_data");
            lock_fram_data();
   		    fram_data.crc = MakeCRC((const char *)&fram_data+sizeof(fram_data.crc),sizeof(fram_data)-sizeof(fram_data.crc));
            old_data = fram_data;
            unlock_fram_data();
            // write the old_data structure to FRAM
            lock_spi();
    		fram_write(&nvram, 12, &old_data, sizeof(old_data));
			unlock_spi();
        }
        if (memcmp(&old_statistics,&fram_statistics,sizeof(fram_statistics))) {
            ESP_LOGD("FRAM","Saved fram_statistics");
            lock_fram_statistics();
   		    fram_statistics.crc = MakeCRC((const char *)&fram_statistics+sizeof(fram_statistics.crc),sizeof(fram_statistics)-sizeof(fram_statistics.crc));
            old_statistics = fram_statistics;
            unlock_fram_statistics();
            // write the old_statistics structure to FRAM
            lock_spi();
    		fram_write(&nvram, 1000, &old_statistics, sizeof(old_statistics));
			unlock_spi();
        }
        if (memcmp(&old_info,&fram_info,sizeof(fram_info))) {
            ESP_LOGD("FRAM","Saved fram_info");
            lock_fram_info();
   		    fram_info.crc = MakeCRC((const char *)&fram_info+sizeof(fram_info.crc),sizeof(fram_info)-sizeof(fram_info.crc));
            old_info = fram_info;
            unlock_fram_info();
            // write the old_info structure to FRAM
            lock_spi();
    		fram_write(&nvram,2000, &old_info, sizeof(old_info));
			unlock_spi();
        }

		// ESP_LOGV("fram_task", "Beat %u", ++i);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}
