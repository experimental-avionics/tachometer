#include "gui.h"

ft81x *_disp;
uint32_t *_p;

screen_page_t gui_current_fg = PAGE_SPLASH;
screen_page_t gui_current_bg = PAGE_NULL;

bool screen_calibrated = false;

int32_t increments10x[] = {100000,10000,1000,100,10,1};
//int32_t increments60x[] = {100000,10000,1000,100,10,1};

esp_err_t gui_get_tag_event(tag_event_t *e);
esp_err_t gui_set_screen(screen_page_t page);
esp_err_t gui_calc();

esp_err_t gui_draw_error();
esp_err_t gui_draw_tach_background		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_draw_tach_foreground		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_draw_splash				(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_run_calibration			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_draw_electrical			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_draw_settings_elec		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_draw_settings_tach		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_draw_settings_info		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);

// Tech access only functions
esp_err_t gui_set_green_arc_low			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_green_arc_high		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_red_line_rpm			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_rpm_settings			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_cruise_rpm			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_hobbs_time			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_tach_time				(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_bat_amps_scale		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_bat_mvolts_scale		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_alt_amps_scale		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_alt_mvolts_scale		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_volt_warn_low			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_volt_warn_high		(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_troubleshoot_tach_sensor	(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_set_max_rpm_drop			(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_view_system_information	(tag_event_t e, uint8_t brightness, uint8_t force_refresh);
esp_err_t gui_view_system_statistics	(tag_event_t e, uint8_t brightness, uint8_t force_refresh);

esp_err_t gui_init(ft81x *disp, uint32_t *p)
{
	gui_calc();

    assert(disp != NULL);
    _disp = disp;
    _p = p;

	lock_spi();
	ft81x_hardware_reset(_disp);

	ft81x_host_command(_disp, CLKEXT, 0);
	ft81x_host_command(_disp, ACTIVE, 0);
	unlock_spi();

	// Wait for the display to power up
	while (true) {
		uint8_t val;
		lock_spi();
		ft81x_rd8(_disp, REG_ID, &val);
		unlock_spi();
		ESP_LOGD("init", "REG_ID=0x%02X", val);
		if (val == 0x7C) break;
		vTaskDelay(5 / portTICK_PERIOD_MS);
	}

	lock_spi();
	ft81x_wr16(_disp, REG_HCYCLE, 928);
	ft81x_wr16(_disp, REG_HOFFSET, 88);
	ft81x_wr16(_disp, REG_HSYNC0, 0);
	ft81x_wr16(_disp, REG_HSYNC1, 48);

	ft81x_wr16(_disp, REG_VCYCLE, 525);
	ft81x_wr16(_disp, REG_VOFFSET, 32);
	ft81x_wr16(_disp, REG_VSYNC0, 0);
	ft81x_wr16(_disp, REG_VSYNC1, 3);

	ft81x_wr8(_disp, REG_PCLK_POL, 0);
	ft81x_wr8(_disp, REG_DITHER, 1);

	ft81x_wr16(_disp, REG_HSIZE, 800);
	ft81x_wr16(_disp, REG_VSIZE, 480);

	ft81x_wr8(_disp, REG_ROTATE, 0);

	ft81x_wr8(_disp, REG_TOUCH_OVERSAMPLE, 15);
	ft81x_wr16(_disp, REG_TOUCH_RZTHRESH, 1200);
	
	p += CMD_DLSTART(p);
	p += CMD_ROMFONT(p, 1, 33);
	p += CMD_ROMFONT(p, 2, 32);
	p += CMD_ROMFONT(p, 3, 34);
	p += CMD_DL(p, CLEAR_COLOR_RGB(0, 0, 0));
	p += CMD_DL(p, CLEAR(1, 1, 1));
	p += CMD_TEXT(p, 400, 240, 31, OPT_CENTER, "PLEASE WAIT...");

	unlock_spi(); // must be unlocked for Update_Display
	UPDATE_DISPLAY
	lock_spi();
	
	ft81x_wr8(_disp, REG_PCLK, 2);
	ft81x_wr16(_disp, REG_GPIOX_DIR, 0xFFFF);
	ft81x_wr16(_disp, REG_GPIOX, 0xFFFF);

	// If the calibration data appears to be good, then write it to the display.
	if (fram_data.transform_a != 0 && fram_data.transform_b != 0 && fram_data.transform_c != 0 && 
		fram_data.transform_d != 0 && fram_data.transform_e != 0 && fram_data.transform_f != 0) {
		ft81x_wr32(_disp, REG_TOUCH_TRANSFORM_A, fram_data.transform_a);
		ft81x_wr32(_disp, REG_TOUCH_TRANSFORM_B, fram_data.transform_b);
		ft81x_wr32(_disp, REG_TOUCH_TRANSFORM_C, fram_data.transform_c);
		ft81x_wr32(_disp, REG_TOUCH_TRANSFORM_D, fram_data.transform_d);
		ft81x_wr32(_disp, REG_TOUCH_TRANSFORM_E, fram_data.transform_e);
		ft81x_wr32(_disp, REG_TOUCH_TRANSFORM_F, fram_data.transform_f);
		ESP_LOGD("gui_init", "Write calibration to display 0x%08X %08X %08X %08X %08X %08X", 
			fram_data.transform_a,fram_data.transform_b,fram_data.transform_c,
			fram_data.transform_d,fram_data.transform_e,fram_data.transform_f);
	
		screen_calibrated = true;
	}
	unlock_spi();

    if (gpio_get_level(BOOTLOAD_PIN) == 0) screen_calibrated = false;

	return ESP_OK;
}

esp_err_t gui_update()
{
	static tag_event_t e = {0}; // Button presses structure
	static typeof(io_data.brightness) _old_brightness = 0; // Keep the old brightness value around
	uint8_t _force_refresh = false;

	if (!screen_calibrated) {
		gui_run_calibration(e,255,1);
		screen_calibrated = true;
	}

	// Check for a button press
	if (gui_get_tag_event(&e) == ESP_OK) {
		// If a valid screenId then change to that screen.
		if (e.state_changed && e.is_down && e.tag && e.tag < MAX_SCREENS) {
			ESP_LOGD("gui", "screen changed from %u to %u", gui_current_fg, e.tag);
			gui_current_fg = (screen_page_t) e.tag;
		}
		_force_refresh = true; // Something changed with the touch interface
	}

	lock_io_data(); // take and give must be around all global variables
	typeof(io_data.brightness) _brightness = io_data.brightness;
	unlock_io_data();

	if (_old_brightness != _brightness) {
		_old_brightness = _brightness;
		_force_refresh = true; // Something changed with brightness
	}

	switch (gui_current_fg) {
		case PAGE_SPLASH:
			gui_draw_splash(e, _brightness, _force_refresh);
			break;
		case PAGE_TACHOMETER:
			if (gui_current_bg != PAGE_TACHOMETER || _force_refresh) {
				// ESP_LOGD("gui", "background changed from %u to %u", gui_current_bg, gui_current_fg);
				gui_draw_tach_background(e, _brightness, _force_refresh);
				gui_current_bg = gui_current_fg;
			}
			gui_draw_tach_foreground(e, _brightness, _force_refresh);
			break;
		case PAGE_ELECTRICAL:
			gui_draw_electrical(e, _brightness, _force_refresh);
			break;
		case PAGE_SETTINGS_ELEC:
			gui_draw_settings_elec(e, _brightness, _force_refresh);
			break;
		case PAGE_SETTINGS_TACH:
			gui_draw_settings_tach(e, _brightness, _force_refresh);
			break;
		case PAGE_SETTINGS_INFO:
			gui_draw_settings_info(e, _brightness, _force_refresh);
			break;
		case PAGE_RUN_CALIBRATION:
			gui_run_calibration(e, _brightness, _force_refresh);
			// Go back to the TACH settings page when calibration is complete
			gui_current_fg = PAGE_SETTINGS_TACH;
			return gui_update();
        case PAGE_SET_GREEN_ARC_LOW:
            gui_set_green_arc_low(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_GREEN_ARC_HIGH:
            gui_set_green_arc_high(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_RED_LINE_RPM:
            gui_set_red_line_rpm(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_RPM_SETTINGS:
            gui_set_rpm_settings(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_CRUISE_RPM:
            gui_set_cruise_rpm(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_HOBBS_TIME:
            gui_set_hobbs_time(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_TACH_TIME:
            gui_set_tach_time(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_BAT_AMPS_SCALE:
            gui_set_bat_amps_scale(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_BAT_MVOLTS_SCALE:
            gui_set_bat_mvolts_scale(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_ALT_AMPS_SCALE:
            gui_set_alt_amps_scale(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_ALT_MVOLTS_SCALE:
            gui_set_alt_mvolts_scale(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_VOLT_WARN_LOW:
            gui_set_volt_warn_low(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_VOLT_WARN_HIGH:	
            gui_set_volt_warn_high(e, _brightness, _force_refresh);
			break;
        case PAGE_SET_MAX_RPM_DROP:
            gui_set_max_rpm_drop(e, _brightness, _force_refresh);
			break;
        case PAGE_SYSTEM_INFORMATION:
            gui_view_system_information(e, _brightness, _force_refresh);
			break;
        case PAGE_SYSTEM_STATISTICS:
            gui_view_system_statistics(e, _brightness, _force_refresh);
			break;
        case PAGE_TROUBLESHOOT_TACH_SENSOR:
			gui_troubleshoot_tach_sensor(e, _brightness, _force_refresh);
			break;
		default:
			gui_draw_error();
			break;
	}

	ESP_LOGV("disp", "Rendered fg=%u bg=%u", gui_current_fg, gui_current_bg);

	return ESP_OK;
}

esp_err_t gui_get_tag_event(tag_event_t *e)
{
	static uint8_t _old_tag_id = 0;
	static uint8_t _old_is_alt = 0xFF;
	// static uint32_t cnt = 0;

	// Default to no changes
	if (e->state_changed) e->state_changed = false;
	// cnt++;

	// Returns 255 if no tag under the pressed area
	// Returns 0 if nothing is touched

	lock_spi();
	ft81x_rd8(_disp, REG_TOUCH_TAG, &e->tag);
	unlock_spi();
	e->is_alt = gpio_get_level(BOOTLOAD_PIN) == 0;

	// Watch for the alt button press
	if (e->is_alt != _old_is_alt) { 	// The button was just pressed or released
		e->state_changed = true;	// Yep, something changed
		_old_is_alt = e->is_alt;  	// Save off the current state for later
		ESP_LOGD("key", "Alt %d Tag %d=%d", e->is_alt, e->tag, e->is_down);
	}

	// Watch for a screen touch or release
	if (_old_tag_id != e->tag) { 	// Something changed between last pass and now
		if (e->tag != 0) {			// something is being touched otherwise it is 0
			if (!e->is_down) {		// There was a different tag under that spot lass pass
			         				// Report the buttonId that was pressed
				e->is_down = true;
				e->state_changed = true;
				ESP_LOGD("key", "Tag %d=%d", e->tag, e->is_down);
			}
		} else { 					// nothing touched now
			if (e->is_down) { 		// And button was down last pass
				e->tag = _old_tag_id;// Report the buttonId that was just released
				e->is_down = false;
				e->state_changed = true;
				ESP_LOGD("key", "Tag %d=%d", e->tag, e->is_down);
			}
		}
		_old_tag_id = e->tag;
	}

	if (e->state_changed) {
		return ESP_OK;
	} else {
		return ESP_FAIL;
	}
}

esp_err_t gui_draw_error()
{
	uint32_t *p = _p;
	p += CMD_DLSTART(p);
	p += CMD_DL(p, CLEAR_COLOR_RGB(0, 0, 0));
	p += CMD_DL(p, CLEAR(1, 1, 1));
	p += CMD_DL(p, LINE_WIDTH(10*16));
	p += CMD_DL(p, COLOR_RED);
	p += CMD_DL(p, BEGIN(LINES));
	p += CMD_DL(p, VERTEX2F(0, 0));
	p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));
	p += CMD_DL(p, VERTEX2F(0, 480 * 16));
	p += CMD_DL(p, VERTEX2F(800 * 16, 0));
	p += CMD_DL(p, END());

	p += CMD_DL(p, SAVE_CONTEXT());
	p += CMD_DL(p, LINE_WIDTH(1 * 16));
	p += CMD_DL(p, COLOR_BLACK);
	p += CMD_DL(p, COLOR_A(192));
	p += CMD_DL(p, BEGIN(RECTS));
	p += CMD_DL(p, VERTEX2F(150 * 16, 185 * 16));
	p += CMD_DL(p, VERTEX2F(650 * 16, 285 * 16));
	p += CMD_DL(p, RESTORE_CONTEXT());

	p += CMD_DL(p, COLOR_WHITE);
	p += CMD_TEXT(p, 400, 215, 31, OPT_CENTER, "UNRECOVERABLE ERROR");
	p += CMD_TEXT(p, 400, 265, 30, OPT_CENTER, "RESTART THIS DEVICE");
	p += CMD_DL(p, DISPLAY());
	p += CMD_DLSWAP(p);
	lock_spi();
	ft81x_bulk_cmd(_disp, _p, p - _p);
	vTaskDelay(50 / portTICK_PERIOD_MS); // Wait 50 mSec for the data to get out the bus
	unlock_spi();
	ft81x_wait_for_queue_empty(_disp, portMAX_DELAY);

	return ESP_OK;
}

esp_err_t gui_calc()
{
	// ESP_LOGD("##dbg", "gui_calc start");
	
	calc_data.green_low_angle = ((270.0f * fram_data.green_arc_low / fram_data.rpm_max_scale - 135.0f) * PI / 180.0f);
	calc_data.green_high_angle = ((270.0f * fram_data.green_arc_high / fram_data.rpm_max_scale - 135.0f) * PI / 180.0f);
	calc_data.green_step_size = ((calc_data.green_high_angle - calc_data.green_low_angle) / ((fram_data.green_arc_high - fram_data.green_arc_low) / 50));
	calc_data.green_high_x = 240 + (182 * sin(calc_data.green_high_angle));
	calc_data.green_high_y = 240 - (182 * cos(calc_data.green_high_angle));
	calc_data.red_line_angle = ((270.0f * fram_data.red_line / fram_data.rpm_max_scale - 135.0f) * PI / 180.0f);
	calc_data.redline_x1 = 240 + (200 * sin(calc_data.red_line_angle));
	calc_data.redline_y1 = 240 - (200 * cos(calc_data.red_line_angle));
	calc_data.redline_x2 = 240 + (160 * sin(calc_data.red_line_angle));
	calc_data.redline_y2 = 240 - (160 * cos(calc_data.red_line_angle));
	calc_data.major_rpm_step_size = (fram_data.rpm_max_scale / MAJOR_TICKS);
	calc_data.tach_time_increment = 60000000 / fram_data.cruise_rpm;
	
	// ESP_LOGD("##dbg", "gui_calc end");
	return ESP_OK;
}
