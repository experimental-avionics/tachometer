#ifndef gui_h
#define gui_h

#include "global.h"
#include <driver/gpio.h>
#include "ft81x.h"
#include "fram.h"

typedef enum {
    PAGE_NULL = 0,
    PAGE_ERROR,
	PAGE_SPLASH,
    PAGE_TACHOMETER,
    PAGE_ELECTRICAL,
    PAGE_SETTINGS_ELEC,
    PAGE_SETTINGS_TACH,
    PAGE_SETTINGS_INFO,
    PAGE_RUN_CALIBRATION,
    PAGE_SET_GREEN_ARC_LOW,
    PAGE_SET_GREEN_ARC_HIGH,
    PAGE_SET_RED_LINE_RPM,
    PAGE_SET_RPM_SETTINGS,
    PAGE_SET_CRUISE_RPM,
    PAGE_SET_HOBBS_TIME,
    PAGE_SET_TACH_TIME,
    PAGE_SET_BAT_AMPS_SCALE,
    PAGE_SET_BAT_MVOLTS_SCALE,
    PAGE_SET_ALT_AMPS_SCALE,
    PAGE_SET_ALT_MVOLTS_SCALE,
    PAGE_SET_VOLT_WARN_LOW,
    PAGE_SET_VOLT_WARN_HIGH,
	PAGE_SET_MAX_RPM_DROP,
	PAGE_TROUBLESHOOT_TACH_SENSOR,
	PAGE_SYSTEM_INFORMATION,
	PAGE_SYSTEM_STATISTICS,
} screen_page_t;

typedef struct {
    // bool down;
    uint8_t is_down;
    uint8_t is_alt;
    uint8_t tag;
    uint8_t state_changed;
} tag_event_t;

extern screen_page_t gui_current_fg;
extern screen_page_t gui_current_bg;
extern ft81x *_disp;
extern uint32_t *_p;
extern int32_t increments10x[];
//extern int32_t increments60x[];

#define BTN_SAVE 254
#define BTN_CANCEL 253

#define COLOR_BLACK COLOR_RGB(0, 0, 0)
#define COLOR_RED   COLOR_RGB(255, 0, 0)
#define COLOR_GREEN COLOR_RGB(0, 128, 0)
#define COLOR_BLUE  COLOR_RGB(0, 0, 128)
#define COLOR_YELLOW COLOR_RGB(255, 128, 0)
#define COLOR_WHITE COLOR_RGB(255, 255, 255)

#define INIT_DISPLAY_BUFFER							\
	uint32_t *p = _p;								\
	p += CMD_DLSTART(p);							\
	p += CMD_DL(p, CLEAR_COLOR_RGB(0, 0, 0));		\
	p += CMD_DL(p, CLEAR(1, 1, 1));

#define PAGE_TITLE(txt) 							\
	p = ScaledGray(p, brightness);					\
	p += CMD_TEXT(p, 400, 450, 29, OPT_CENTERX, txt);

#define UPDATE_DISPLAY								\
	p += CMD_DL(p, DISPLAY());						\
	p += CMD_DLSWAP(p);								\
	lock_spi();										\
	ft81x_bulk_cmd(_disp, _p, p - _p);				\
	ft81x_wait_for_queue_empty(_disp, 500 / portTICK_PERIOD_MS); \
	unlock_spi();

#define BUTTON_ROW_1 0
#define BUTTON_ROW_2 115
#define BUTTON_ROW_3 230
#define BUTTON_ROW_4 345

#define BUTTON_COL_1 100
#define BUTTON_COL_2 425

#define MAKE_2x4_BUTTON(tag,x,y,txt) 				\
	p += CMD_DL(p, TAG(tag)); 						\
	p += CMD_BUTTON(p, x, y, 275, 100, 29, 0, txt);

#define MAKE_3x4_BUTTON(tag,x,y,txt1, txt2)			\
	p += CMD_DL(p, TAG(tag)); 						\
	p += CMD_BUTTON(p, x, y, 180, 100, 29, 0, "");  \
	p += CMD_TEXT(p, x+90, y+30, 29, OPT_CENTER, txt1);\
	p += CMD_TEXT(p, x+90, y+60, 29, OPT_CENTER, txt2);

#define DRAW_PAGE_LEFT(tag)							\
	p = ScaledGray(p, brightness);					\
	p += CMD_DL(p, LINE_WIDTH(3 * 16));				\
	p += CMD_DL(p, BEGIN(LINE_STRIP));				\
	p += CMD_DL(p, VERTEX2F(30 * 16, 200 * 16));	\
	p += CMD_DL(p, VERTEX2F(10 * 16, 240 * 16));	\
	p += CMD_DL(p, VERTEX2F(30 * 16, 280 * 16));	\
	p += CMD_DL(p, SAVE_CONTEXT());					\
	p += CMD_DL(p, LINE_WIDTH(1 * 16));				\
	p += CMD_DL(p, COLOR_WHITE);					\
	p += CMD_DL(p, COLOR_A(00));					\
	p += CMD_DL(p, BEGIN(RECTS));					\
	p += CMD_DL(p, TAG(tag));						\
	p += CMD_DL(p, VERTEX2F(0 * 16, 0 * 16));		\
	p += CMD_DL(p, VERTEX2F(100 * 16, 480 * 16));	\
	p += CMD_DL(p, RESTORE_CONTEXT());

#define DRAW_PAGE_RIGHT(tag)						\
	p = ScaledGray(p, brightness);					\
	p += CMD_DL(p, LINE_WIDTH(3 * 16));				\
	p += CMD_DL(p, BEGIN(LINE_STRIP));				\
	p += CMD_DL(p, VERTEX2F(770 * 16, 200 * 16));	\
	p += CMD_DL(p, VERTEX2F(790 * 16, 240 * 16));	\
	p += CMD_DL(p, VERTEX2F(770 * 16, 280 * 16));	\
	p += CMD_DL(p, SAVE_CONTEXT());					\
	p += CMD_DL(p, LINE_WIDTH(1 * 16));				\
	p += CMD_DL(p, COLOR_WHITE);					\
	p += CMD_DL(p, COLOR_A(00));					\
	p += CMD_DL(p, BEGIN(RECTS));					\
	p += CMD_DL(p, TAG(tag));			 			\
	p += CMD_DL(p, VERTEX2F(700 * 16, 0 * 16));		\
	p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));	\
	p += CMD_DL(p, RESTORE_CONTEXT());

#define PI 3.1415926535897932384626433832795f
#define MAJOR_TICKS 7
#define MINOR_TICKS 5

esp_err_t gui_init(ft81x *disp, uint32_t *buf);
esp_err_t gui_update();
esp_err_t gui_calc();

esp_err_t gui_set_generic_value(tag_event_t e, uint8_t brightness, uint8_t force_refresh, int32_t *gen_value, int32_t *old_value,
		 int32_t orig_value, int32_t *increments, char * fmt_string, double scaler,
		 int32_t max_digits, uint8_t return_page_id, char * page_title);

esp_err_t gui_set_generic_time (tag_event_t e, uint8_t brightness, uint8_t force_refresh, uint64_t *gen_time, uint64_t *old_time,
		 char * page_title);

#endif