#include "gui.h"
#include "framdata.h"

esp_err_t gui_run_calibration(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	// Just switching to this page, wait for button release
	if (e.tag == PAGE_RUN_CALIBRATION && e.is_down) return ESP_OK;

	INIT_DISPLAY_BUFFER

	p += CMD_DL(p, COLOR_WHITE);
	p += CMD_TEXT(p, 400, 215, 31, OPT_CENTER, "TOUCHSCREEN CALIBRATION");
	p += CMD_TEXT(p, 400, 265, 30, OPT_CENTER, "TAP ON THE DOT");
	p += CMD_CALIBRATE(p);
	p += CMD_DLSWAP(p);
	lock_spi();
	ft81x_bulk_cmd(_disp, _p, p - _p);
	vTaskDelay(50 / portTICK_PERIOD_MS); // Wait 50 mSec for the data to get out the bus
	unlock_spi();
	ft81x_wait_for_queue_empty(_disp, portMAX_DELAY); // Wait forever for the calibration to complete

	// Read calibration values from display
	lock_spi();
	lock_fram_data();
	ESP_ERROR_CHECK(ft81x_rd32(_disp, REG_TOUCH_TRANSFORM_A, &fram_data.transform_a));
	ESP_ERROR_CHECK(ft81x_rd32(_disp, REG_TOUCH_TRANSFORM_B, &fram_data.transform_b));
	ESP_ERROR_CHECK(ft81x_rd32(_disp, REG_TOUCH_TRANSFORM_C, &fram_data.transform_c));
	ESP_ERROR_CHECK(ft81x_rd32(_disp, REG_TOUCH_TRANSFORM_D, &fram_data.transform_d));
	ESP_ERROR_CHECK(ft81x_rd32(_disp, REG_TOUCH_TRANSFORM_E, &fram_data.transform_e));
	ESP_ERROR_CHECK(ft81x_rd32(_disp, REG_TOUCH_TRANSFORM_F, &fram_data.transform_f));
	unlock_spi();
	unlock_fram_data();
	vTaskDelay(5 / portTICK_PERIOD_MS); // Wait 5 mSec anything else to use the bus

	ESP_LOGD("gui_run_calibration", "Display Calibration 0x%08X %08X %08X %08X %08X %08X", 
		fram_data.transform_a,fram_data.transform_b,fram_data.transform_c,
		fram_data.transform_d,fram_data.transform_e,fram_data.transform_f);
	
	return ESP_OK;
}
