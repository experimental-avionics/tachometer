#include "gui.h"

esp_err_t gui_set_bat_amps_scale(tag_event_t e, uint8_t brightness, uint8_t force_refresh)     
{
	static int32_t _temp_var;
	static int32_t _old_temp_var = 0xFFFFFFFE;

	// Just switching to this page, wait for button release
	if (e.tag == PAGE_SET_BAT_AMPS_SCALE && e.is_down) {
		lock_fram_data(); // take and give must be around all global variables
		_temp_var = fram_data.bat_amps;	// read and write
		unlock_fram_data();
		return ESP_OK;
	} 

	lock_fram_data(); // take and give must be around all global variables
	int32_t _current_var = fram_data.bat_amps;
	unlock_fram_data();

	if (gui_set_generic_value(e,brightness,force_refresh,&_temp_var,&_old_temp_var,_current_var,
		increments10x,"%5.0f",1.0, 4, PAGE_SETTINGS_ELEC,
		"SET MAX BAT AMPS") == BTN_SAVE) {
		// ESP_LOGD("gui", "Data Saved");
		lock_fram_data(); 					// take and give must be around all global variables
		fram_data.bat_amps = _temp_var;	// Save off the changed value
		unlock_fram_data();
	}
	return ESP_OK;
}
