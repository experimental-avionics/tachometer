#include "gui.h"

esp_err_t gui_set_generic_time(tag_event_t e, uint8_t brightness, uint8_t force_refresh, uint64_t *gen_time, uint64_t *old_time,
		 char * page_title)
{
	if (e.state_changed && e.is_down) {
		// ESP_LOGD("gui", "Tag %u down", e.tag);
		// These tagIds are blindly coordinated with the code below that creates the buttons
		// Note: 1000000 = 1 second
		switch (e.tag) {
			case MAX_SCREENS+ 0:	*gen_time += 3600000000000;	break; // thousands of hours
			case MAX_SCREENS+ 1:	*gen_time -= 3600000000000;	break;
			case MAX_SCREENS+ 2:	*gen_time += 360000000000;	break; // hundreds of hours
			case MAX_SCREENS+ 3:	*gen_time -= 360000000000;	break;
			case MAX_SCREENS+ 4:	*gen_time += 36000000000;	break; // tens of hours
			case MAX_SCREENS+ 5:	*gen_time -= 36000000000;	break;
			case MAX_SCREENS+ 6:	*gen_time += 3600000000;	break; // hours
			case MAX_SCREENS+ 7:	*gen_time -= 3600000000;	break;
			case MAX_SCREENS+ 8:	*gen_time += 360000000;		break; // tenths of hours
			case MAX_SCREENS+ 9:	*gen_time -= 360000000;		break;
			case MAX_SCREENS+10:	*gen_time += 36000000;		break; // hundreths of hours
			case MAX_SCREENS+11:	*gen_time -= 36000000;		break;
			case MAX_SCREENS+12: // Save
			    gui_current_fg = PAGE_SETTINGS_TACH;
				return BTN_SAVE;
			break;
			case MAX_SCREENS+13: // Cancel
			    gui_current_fg = PAGE_SETTINGS_TACH;
				return BTN_CANCEL;
			break;
		}
		if (*gen_time > 360000000000000) *gen_time = 0; 	// Limit time between 0 and 100k hours
	}

	// Forced refresh requested
	if (force_refresh) *old_time = 0xFFFFFFFFFFFFFFFE;

	if (*gen_time != *old_time) { 				// Don't update the display unless something changed
		INIT_DISPLAY_BUFFER
		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

		PAGE_TITLE(page_title);			// Write the page title

		p += CMD_DL(p, COLOR_WHITE); 			// Write the number in white
		
		// Write the digits across the screen, spaced as needed
		char disp_buf[16];
		snprintf(disp_buf, sizeof(disp_buf), "%0.2f", (double)(*gen_time * 100 / 1000000 / 3600) / 100.0);
		char * pt = disp_buf;					// temp pointer
		int16_t loc = 550;						// The starting location for the display
		while (*pt++);  						// Find the null at the end
		pt--;									// Step back to the null
		while (pt != disp_buf) {
			if (*--pt == '.') { 				// if this is a dot, put it on the half space
				p += CMD_TEXT(p, loc+50, 220, 3, OPT_CENTER, pt);
				loc += 100;						// Move the location back, so the next char is correct
			} else {
				if (loc < 100) pt = disp_buf; 	// Dump the rest of the string in the first digit
				p += CMD_TEXT(p, loc, 220, 3, OPT_CENTER, pt);
			}
			*pt = 0;
			loc -=100;
		}

		// Set line width and color for arrows
		p = ScaledGray(p, brightness);
		p += CMD_DL(p, LINE_WIDTH(3 * 16));
		for (int i = 0; i < 600; i += 100) {
			p += CMD_DL(p, BEGIN(LINE_STRIP));	// Draw Upper button arrows
			p += CMD_DL(p, VERTEX2F((i+10) * 16, 120 * 16));
			p += CMD_DL(p, VERTEX2F((i+50) * 16,  70 * 16));
			p += CMD_DL(p, VERTEX2F((i+90) * 16, 120 * 16));
			p += CMD_DL(p, BEGIN(LINE_STRIP));	// Draw Lower button arrows
			p += CMD_DL(p, VERTEX2F((i+10) * 16, 320 * 16));
			p += CMD_DL(p, VERTEX2F((i+50) * 16, 370 * 16));
			p += CMD_DL(p, VERTEX2F((i+90) * 16, 320 * 16));
		}

		p += CMD_DL(p, SAVE_CONTEXT());
		p += CMD_DL(p, LINE_WIDTH(1 * 16));
		p += CMD_DL(p, COLOR_WHITE);			// Pick a color / any color
		p += CMD_DL(p, COLOR_A(0)); 			// Make them clear buttons
		p += CMD_DL(p, BEGIN(RECTS));

		int tagId = MAX_SCREENS;				// Starting Tag ID for buttons
		for (int i = 0; i < 600; i += 100) {
			p += CMD_DL(p, TAG(tagId++));		// Make upper tags
			p += CMD_DL(p, VERTEX2F((i + 05) * 16, 0 * 16));
			p += CMD_DL(p, VERTEX2F((i + 95) * 16, 180 * 16));
			p += CMD_DL(p, TAG(tagId++));		// Make lower tags
			p += CMD_DL(p, VERTEX2F((i + 05) * 16, 260 * 16));
			p += CMD_DL(p, VERTEX2F((i + 95) * 16, 430 * 16));
		}
		
		p += CMD_DL(p, COLOR_GREEN);			// Save Button
		p += CMD_DL(p, COLOR_A(255));
		p += CMD_DL(p, TAG(tagId++));
		p += CMD_DL(p, VERTEX2F(620 * 16, 0 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 240 * 16));
		p += CMD_DL(p, COLOR_RED);				// Cancel Button
		p += CMD_DL(p, TAG(tagId++));
		p += CMD_DL(p, VERTEX2F(620 * 16, 241 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));

		p += CMD_DL(p, RESTORE_CONTEXT());

		p += CMD_DL(p, COLOR_WHITE); 			// Write the labels in white
		p += CMD_TEXT(p, 710, 120, 29, OPT_CENTER, "SAVE");
		p += CMD_TEXT(p, 710, 360, 29, OPT_CENTER, "CANCEL");

		UPDATE_DISPLAY
	}

	return ESP_OK;
}
