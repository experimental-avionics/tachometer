#include "gui.h"

esp_err_t gui_set_generic_value(tag_event_t e, uint8_t brightness, uint8_t force_refresh, int32_t *gen_value, int32_t *old_value,
		 int32_t orig_value, int32_t *increments, char * fmt_string, double scaler,
		 int32_t max_digits, uint8_t return_page_id, char * page_title)
{
	if (e.state_changed && e.is_down) {
		// ESP_LOGD("gui", "Tag %u down", e.tag);
		// These tagIds are blindly coordinated with the code below that creates the buttons
		// Note: 1000000 = 1 second
		switch (e.tag) {
			case MAX_SCREENS+ 0:	*gen_value += increments[0];	break; 
			case MAX_SCREENS+ 1:	*gen_value -= increments[0];	break;
			case MAX_SCREENS+ 2:	*gen_value += increments[1];	break; 
			case MAX_SCREENS+ 3:	*gen_value -= increments[1];	break;
			case MAX_SCREENS+ 4:	*gen_value += increments[2];	break; 
			case MAX_SCREENS+ 5:	*gen_value -= increments[2];	break;
			case MAX_SCREENS+ 6:	*gen_value += increments[3];	break; 
			case MAX_SCREENS+ 7:	*gen_value -= increments[3];	break;
			case MAX_SCREENS+ 8:	*gen_value += increments[4];	break; 
			case MAX_SCREENS+ 9:	*gen_value -= increments[4];	break;
			case MAX_SCREENS+10:	*gen_value += increments[5];	break; 
			case MAX_SCREENS+11:	*gen_value -= increments[5];	break;
			case MAX_SCREENS+12: // Save
			    gui_current_fg = return_page_id;
				return BTN_SAVE;
			break;
			case MAX_SCREENS+13: // Cancel
			    gui_current_fg = return_page_id;
				return BTN_CANCEL;
			break;
		}
		// if (*gen_value > 360000000000000) *gen_value = 0; 	// Limit time between 0 and 100k hours
		if (*gen_value < 0) *gen_value = 0; 	// no negative values
	}

	// Forced refresh requested
	if (force_refresh) *old_value = 0xFFFFFFFE;

	if (*gen_value != *old_value) { 		// Don't update the display unless something changed
		*old_value = *gen_value;
		INIT_DISPLAY_BUFFER
		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

		PAGE_TITLE(page_title);			// Write the page title

		p += CMD_DL(p, COLOR_WHITE); 			// Write the number in white
		
		// Write the digits across the screen, spaced as needed
		char disp_buf[16];
		snprintf(disp_buf, sizeof(disp_buf), fmt_string, (double)(*gen_value) * scaler);
		char * pt = disp_buf;					// temp pointer
		int16_t loc = 550;						// The starting location for the display
		while (*pt++);  						// Find the null at the end
		pt--;									// Step back to the null
		while (pt != disp_buf) {
			if (*--pt == '.') { 				// if this is a dot, put it on the half space
				p += CMD_TEXT(p, loc+50, 220, 3, OPT_CENTER, pt);
				loc += 100;						// Move the location back, so the next char is correct
			} else {
				if (loc < 100) pt = disp_buf; 	// Dump the rest of the string in the first digit
				p += CMD_TEXT(p, loc, 220, 3, OPT_CENTER, pt);
			}
			*pt = 0;
			loc -=100;
		}

		snprintf(disp_buf, sizeof(disp_buf), fmt_string, (double)(orig_value) * scaler);
		p += CMD_TEXT(p, 10,450,29,OPT_CENTERY,disp_buf);

		// Set line width and color for arrows
		p = ScaledGray(p, brightness);
		p += CMD_DL(p, LINE_WIDTH(3 * 16));
		for (int i = 0, dig = 6; i < 600; i += 100, dig--) {
			if (dig > max_digits) continue;
			p += CMD_DL(p, BEGIN(LINE_STRIP));	// Draw Upper button arrows
			p += CMD_DL(p, VERTEX2F((i+10) * 16, 120 * 16));
			p += CMD_DL(p, VERTEX2F((i+50) * 16,  70 * 16));
			p += CMD_DL(p, VERTEX2F((i+90) * 16, 120 * 16));
			p += CMD_DL(p, BEGIN(LINE_STRIP));	// Draw Lower button arrows
			p += CMD_DL(p, VERTEX2F((i+10) * 16, 320 * 16));
			p += CMD_DL(p, VERTEX2F((i+50) * 16, 370 * 16));
			p += CMD_DL(p, VERTEX2F((i+90) * 16, 320 * 16));
		}

		p += CMD_DL(p, SAVE_CONTEXT());
		p += CMD_DL(p, LINE_WIDTH(1 * 16));
		p += CMD_DL(p, COLOR_WHITE);			// Pick a color / any color
		p += CMD_DL(p, COLOR_A(0)); 			// Make them clear buttons
		p += CMD_DL(p, BEGIN(RECTS));

		int tagId = MAX_SCREENS;				// Starting Tag ID for buttons
		for (int i = 0, dig = 6; i < 600; i += 100, dig--) {
			if (dig > max_digits) {
				tagId += 2;
				continue;
			}
			p += CMD_DL(p, TAG(tagId++));		// Make upper tags
			p += CMD_DL(p, VERTEX2F((i + 05) * 16, 0 * 16));
			p += CMD_DL(p, VERTEX2F((i + 95) * 16, 180 * 16));
			p += CMD_DL(p, TAG(tagId++));		// Make lower tags
			p += CMD_DL(p, VERTEX2F((i + 05) * 16, 260 * 16));
			p += CMD_DL(p, VERTEX2F((i + 95) * 16, 430 * 16));
		}

		p += CMD_DL(p, COLOR_GREEN);			// Save Button
		p += CMD_DL(p, COLOR_A(255));
		p += CMD_DL(p, TAG(tagId++));
		p += CMD_DL(p, VERTEX2F(620 * 16, 0 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 240 * 16));
		p += CMD_DL(p, COLOR_RED);				// Cancel Button
		p += CMD_DL(p, TAG(tagId++));
		p += CMD_DL(p, VERTEX2F(620 * 16, 241 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));

		p += CMD_DL(p, RESTORE_CONTEXT());

		p += CMD_DL(p, COLOR_WHITE); 			// Write the labels in white
		p += CMD_TEXT(p, 710, 120, 29, OPT_CENTER, "SAVE");
		p += CMD_TEXT(p, 710, 360, 29, OPT_CENTER, "CANCEL");

		UPDATE_DISPLAY
	}

	return ESP_OK;
}
