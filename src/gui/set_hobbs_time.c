#include "gui.h"
#include "framdata.h"

esp_err_t gui_set_hobbs_time(tag_event_t e, uint8_t brightness, uint8_t force_refresh)         
{
	static uint64_t _temp_var;
	static uint64_t _old_temp_var = 0xFFFFFFFFFFFFFFFE;

	// Just switching to this page, wait for button release
	if (e.tag == PAGE_SET_HOBBS_TIME && e.is_down) {
		lock_fram_data(); // take and give must be around all global variables
		_temp_var = fram_data.hobbs_time;	// read and write
		unlock_fram_data();
		return ESP_OK;
	} 

	if (gui_set_generic_time(e,brightness,force_refresh,&_temp_var,&_old_temp_var,"ALTER HOBBS TIME") == BTN_SAVE) {
		lock_fram_data(); 					// take and give must be around all global variables
		fram_data.hobbs_time = _temp_var;	// Save off the changed value
		unlock_fram_data();
	}
	return ESP_OK;
}
