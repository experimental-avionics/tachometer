#include "gui.h"

esp_err_t gui_set_rpm_settings(tag_event_t e, uint8_t brightness, uint8_t force_refresh)          
{
	// Max RPMs, RPM smoothing, Pulses per revelution
	// static int32_t _current_rpm_max_scale;
	// static int32_t _current_rpm_smoothing;
	// static int32_t _current_pulses_per_rev;

	static int32_t _new_rpm_max_scale;
	static int32_t _new_rpm_smoothing;
	static int32_t _new_pulses_per_rev;

	// Just switching to this page, wait for button release
	if (e.tag == PAGE_SET_RPM_SETTINGS && e.is_down) {
		lock_fram_data(); // take and give must be around all global variables
		// Save off current values
		// _current_rpm_max_scale = 
		_new_rpm_max_scale = fram_data.rpm_max_scale;
		// _current_rpm_smoothing = 
		_new_rpm_smoothing = fram_data.rpm_smoothing;
		// _current_pulses_per_rev = 
		_new_pulses_per_rev = fram_data.pulses_per_rev;
		unlock_fram_data();
		return ESP_OK;
	}

	// lockxx(); // take and give must be around all global variables
	// unlockxx();

	if (e.state_changed && e.is_down) {
		// ESP_LOGD("gui", "Tag %u down", e.tag);
		switch (e.tag) {
			case 240:
				if (_new_rpm_max_scale == 3500) _new_rpm_max_scale = 7000;
				else _new_rpm_max_scale = 3500;
				force_refresh = true;
				break;
			case 241:
				if (++_new_rpm_smoothing > 3) _new_rpm_smoothing = 1;
				force_refresh = true;
				break;
			case 242:
				if (++_new_pulses_per_rev > 4) _new_pulses_per_rev = 1;
				force_refresh = true;
				break;
			case 250: // Save
				// Save the temp data to the fram structure
				lock_fram_data(); // take and give must be around all global variables
				fram_data.rpm_max_scale = _new_rpm_max_scale;
				fram_data.rpm_smoothing = _new_rpm_smoothing;
				fram_data.pulses_per_rev = _new_pulses_per_rev;
				unlock_fram_data();
			    gui_current_fg = PAGE_SETTINGS_TACH;
				return ESP_OK;
			case 251: // Cancel
			    gui_current_fg = PAGE_SETTINGS_TACH;
				return ESP_OK;
		}
		// if (*gen_value < 0) *gen_value = 0; 	// no negative values
	}

	if (force_refresh) {
		INIT_DISPLAY_BUFFER
		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);
		PAGE_TITLE("RPM SETTINGS");	// Write the page title

		char buf1[16], buf2[16];
		snprintf(buf1, sizeof(buf1), "%d", _new_rpm_max_scale);
		snprintf(buf2, sizeof(buf2), "%d", _new_pulses_per_rev);

		p += CMD_TEXT(p,  50, 75, 30, OPT_CENTERY, "FULL SCALE RPM");
		p += CMD_TEXT(p, 325, 75,  2, OPT_CENTERY, buf1);
		p += CMD_TEXT(p,  50, 175, 30, OPT_CENTERY, "RPM SMOOTHING");

		switch(_new_rpm_smoothing) {
			default: p += CMD_TEXT(p, 325, 175,  2, OPT_CENTERY, "XXXX"); break;
			case 1:  p += CMD_TEXT(p, 325, 175,  2, OPT_CENTERY, "LOW"); break;
			case 2:  p += CMD_TEXT(p, 325, 175,  2, OPT_CENTERY, "NORMAL"); break;
			case 3:  p += CMD_TEXT(p, 325, 175,  2, OPT_CENTERY, "HIGH"); break;
		}
		
		p += CMD_TEXT(p,  50, 275, 30, OPT_CENTERY, "PULSES PER REV");
		p += CMD_TEXT(p, 325, 275,  2, OPT_CENTERY, buf2);

		p += CMD_DL(p, SAVE_CONTEXT());
		p += CMD_DL(p, BEGIN(RECTS));
		p += CMD_DL(p, COLOR_A(60));
		p += CMD_DL(p, COLOR_BLUE);
		p += CMD_DL(p, LINE_WIDTH(1 * 16));
		p += CMD_DL(p, TAG(240));
		p += CMD_DL(p, VERTEX2F(  0 * 16,  25 * 16));
		p += CMD_DL(p, VERTEX2F(600 * 16, 120 * 16));
		p += CMD_DL(p, TAG(241));
		p += CMD_DL(p, VERTEX2F(  0 * 16, 125 * 16));
		p += CMD_DL(p, VERTEX2F(600 * 16, 220 * 16));
		p += CMD_DL(p, TAG(242));
		p += CMD_DL(p, VERTEX2F(  0 * 16, 225 * 16));
		p += CMD_DL(p, VERTEX2F(600 * 16, 320 * 16));

		p += CMD_DL(p, COLOR_GREEN);			// Save Button
		p += CMD_DL(p, COLOR_A(255));
		p += CMD_DL(p, TAG(250));
		p += CMD_DL(p, VERTEX2F(620 * 16, 0 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 240 * 16));
		p += CMD_DL(p, COLOR_RED);				// Cancel Button
		p += CMD_DL(p, TAG(251));
		p += CMD_DL(p, VERTEX2F(620 * 16, 241 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));
		p += CMD_DL(p, RESTORE_CONTEXT());

		p += CMD_DL(p, COLOR_WHITE); 			// Write the labels in white
		p += CMD_TEXT(p, 710, 120, 29, OPT_CENTER, "SAVE");
		p += CMD_TEXT(p, 710, 360, 29, OPT_CENTER, "CANCEL");

		UPDATE_DISPLAY
	}
	return ESP_OK;
}
