#include "gui.h"

esp_err_t gui_troubleshoot_tach_sensor(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	static uint32_t _old_tach_mode = 0xFFFF;

	// Just switching to this page, wait for button release
	if (e.tag == PAGE_TROUBLESHOOT_TACH_SENSOR && e.is_down) {
		return ESP_OK;
	} 

	lock_io_data(); // take and give must be around all global variables
	lock_tach_data();
	uint32_t _tach_mode = io_data.tach_mode;
	tach_data_v1_t _tach_data = tach_data;
	unlock_io_data();
	unlock_tach_data();

	if (e.state_changed && e.is_down) {
		// ESP_LOGD("gui", "Tag %u down", e.tag);
		switch (e.tag) {
			case 240: gui_current_fg = PAGE_SETTINGS_TACH; return BTN_CANCEL;
			case 241:
				lock_io_data(); // take and give must be around all global variables
				if (io_data.tach_mode == TACH_MODE_NORMAL) {
					tach_data.tach_index = 0;
					io_data.tach_mode = TACH_MODE_TROUBLESHOOT;
				}
				unlock_io_data();
				break;
			case 242:
				lock_io_data(); // take and give must be around all global variables
				if (io_data.tach_mode == TACH_MODE_TROUBLESHOOT) {
					force_refresh = true;
					io_data.tach_mode = TACH_MODE_NORMAL;
				}
				unlock_io_data();
				break;
		}
	}

	if (force_refresh || 
	    (_old_tach_mode != _tach_mode) ||
		(tach_data.tach_index > 0)) {

		INIT_DISPLAY_BUFFER
		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

		PAGE_TITLE("TROUBLESHOOT TACH SENSOR");	// Write the page title

		// Draw the oScope image
		p += CMD_DL(p, COLOR_WHITE);
		p += CMD_DL(p, COLOR_A(255));
		p += CMD_DL(p, LINE_WIDTH(1 * 16));
		p += CMD_DL(p, BEGIN(LINE_STRIP));
		p += CMD_DL(p, VERTEX2F(  1 * 16,   0 * 16));
		p += CMD_DL(p, VERTEX2F(799 * 16,   0 * 16));
		p += CMD_DL(p, VERTEX2F(799 * 16, 290 * 16));
		p += CMD_DL(p, VERTEX2F(  1 * 16, 290 * 16));
		p += CMD_DL(p, VERTEX2F(  1 * 16,   0 * 16));

		if (_tach_data.tach_index > 1) { 			// there is some data to display
			uint32_t _total_delta = 0;
			_tach_data.tach_delta[0] = 0; 			// Skip the first sample time, it is bogus
			for (int i = 0; i < _tach_data.tach_index; i++) {
				_total_delta += _tach_data.tach_delta[i];				
			}
			if (_total_delta > 0) {					// We have a time scale to display
				p += CMD_DL(p, COLOR_RED);
				p += CMD_DL(p, LINE_WIDTH(2 * 16));
				p += CMD_DL(p, BEGIN(LINE_STRIP));
				uint32_t graph_sum_delta = 0;
				static uint32_t _old_graph_x = 0;
				_old_graph_x = 5 * 16; 				// start out on the left side
				if (_tach_data.tach_state[0]) {		// first point is high
					p += CMD_DL(p, VERTEX2F( _old_graph_x, 285 * 16));    	// Low
					p += CMD_DL(p, VERTEX2F( _old_graph_x,   5 * 16));    	// High
				} else {
					p += CMD_DL(p, VERTEX2F( _old_graph_x,   5 * 16));    	// High
					p += CMD_DL(p, VERTEX2F( _old_graph_x, 285 * 16));    	// Low
				}
				for (int i = 1; i < _tach_data.tach_index; i++) {
					graph_sum_delta += _tach_data.tach_delta[i];
					uint32_t _graph_x = 5 * 16 + (uint32_t)((790*16.0*graph_sum_delta)/_total_delta);
					if (_tach_data.tach_state[i]) { // just went high
						p += CMD_DL(p, VERTEX2F( _graph_x, 285 * 16));  	// Low
						p += CMD_DL(p, VERTEX2F( _graph_x,   5 * 16));    	// High
					} else {						
						p += CMD_DL(p, VERTEX2F( _graph_x,   5 * 16));	 	// High
						p += CMD_DL(p, VERTEX2F( _graph_x, 285 * 16)); 	    // Low
					}
					_old_graph_x = _graph_x;
				}
			}
		}

		p += CMD_DL(p, SAVE_CONTEXT());
		p += CMD_DL(p, COLOR_RED);				// Cancel Button
		p += CMD_DL(p, COLOR_A(255));
		p += CMD_DL(p, BEGIN(RECTS));
		p += CMD_DL(p, TAG(240));
		p += CMD_DL(p, VERTEX2F(600 * 16, 360 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));
		p += CMD_DL(p, COLOR_GREEN);			// Collect Button
		if (_tach_mode == TACH_MODE_NORMAL) {
			p += CMD_DL(p, TAG(241));
		} else {
			p += CMD_DL(p, TAG(242));
		}
		p += CMD_DL(p, VERTEX2F(  0 * 16, 360 * 16));
		p += CMD_DL(p, VERTEX2F(200 * 16, 480 * 16));
		p += CMD_DL(p, RESTORE_CONTEXT());

		p += CMD_DL(p, COLOR_WHITE); 			// Write the labels in white
		p += CMD_TEXT(p, 700, 420, 29, OPT_CENTER, "CANCEL");
		p += CMD_TEXT(p, 100, 440, 29, OPT_CENTER, "COLLECT");

		if (_tach_mode == TACH_MODE_NORMAL) {
			p += CMD_TEXT(p, 100, 400, 29, OPT_CENTER, "START");
		} else {
			p += CMD_TEXT(p, 100, 400, 29, OPT_CENTER, "END");
		}

		// p += CMD_SETROTATE(p,2);  // Rotate does the entire screen, not just a part.
		// p += CMD_TEXT(p, 100,100,29,OPT_DEFAULT,disp_buf1);
		// p += CMD_TEXT(p, 100,150,29,OPT_DEFAULT,disp_buf2);
		// p += CMD_TEXT(p, 100,200,29,OPT_DEFAULT,disp_buf3);
		// p += CMD_TEXT(p, 100,250,29,OPT_DEFAULT,disp_buf4);
		// p += CMD_SETROTATE(p,0);

		UPDATE_DISPLAY

		_old_tach_mode = _tach_mode;
	}

	return ESP_OK;	
}