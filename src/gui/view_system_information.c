#include "gui.h"

esp_err_t gui_view_system_information(tag_event_t e, uint8_t brightness, uint8_t force_refresh)
{
	// Just switching to this page, wait for button release
	if (e.tag == PAGE_SYSTEM_INFORMATION && e.is_down) {
		return ESP_OK;
	}

	lock_fram_info(); // take and give must be around all global variables
	fram_module_info_v1_t _fram_info = fram_info;
	unlock_fram_info();

	if (e.state_changed && e.is_down) {
		// ESP_LOGD("gui", "Tag %u down", e.tag);
		switch (e.tag) {
			case BTN_CANCEL: gui_current_fg = PAGE_SETTINGS_TACH; return BTN_CANCEL;
			case 233: gui_current_fg = PAGE_SETTINGS_ELEC; return BTN_CANCEL;
		}
	}

	if (force_refresh) {
		INIT_DISPLAY_BUFFER
		p += CMD_MEMWRITE(p, REG_PWM_DUTY, brightness);

		PAGE_TITLE("SYSTEM INFORMATION");	// Write the page title

		char buf1[10] = {0};
		char buf2[10];
		char buf3[10];
		snprintf(buf1, sizeof(buf1), "Tach");
		snprintf(buf2, sizeof(buf2), "%u", _fram_info.module_version);
		snprintf(buf3, sizeof(buf3), "%u", _fram_info.module_serial);

		p += CMD_TEXT(p,  50,  50, 30, OPT_CENTERY, "Module type:");
		p += CMD_TEXT(p,  50, 100, 30, OPT_CENTERY, "Module version:");
		p += CMD_TEXT(p,  50, 150, 30, OPT_CENTERY, "Module serial number:");

		p += CMD_TEXT(p, 360,  50, 30, OPT_CENTERY, buf1);
		p += CMD_TEXT(p, 360, 100, 30, OPT_CENTERY, buf2);
		p += CMD_TEXT(p, 360, 150, 30, OPT_CENTERY, buf3);

		p += CMD_DL(p, SAVE_CONTEXT());
		p += CMD_DL(p, COLOR_RED);				// Cancel Button
		p += CMD_DL(p, COLOR_A(255));
		p += CMD_DL(p, BEGIN(RECTS));
		p += CMD_DL(p, TAG(BTN_CANCEL));
		p += CMD_DL(p, VERTEX2F(600 * 16, 360 * 16));
		p += CMD_DL(p, VERTEX2F(800 * 16, 480 * 16));
		if (e.is_alt) {
			p += CMD_DL(p, COLOR_A(33));
			p += CMD_DL(p, TAG(233));
			p += CMD_DL(p, VERTEX2F(600 * 16,   0 * 16));
			p += CMD_DL(p, VERTEX2F(800 * 16, 350 * 16));
		}
		p += CMD_DL(p, RESTORE_CONTEXT());

		p += CMD_DL(p, COLOR_WHITE); 			// Write the labels in white
		p += CMD_TEXT(p, 700, 420, 29, OPT_CENTER, "CANCEL");

		UPDATE_DISPLAY
	}

	return ESP_OK;	
}
