#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

#include <string.h>
#include <math.h>
#include "sdkconfig.h"
#include <driver/gpio.h>
#include <driver/adc.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include <esp_adc_cal.h>
#include "global.h"
#include "ft81x.h"
#include "fram.h"
#include "gui/gui.h"
#include "os_status.h"
#include "gui/framdata.h"

ft81x display;
uint32_t cmd_buf[4096 / 4];

esp_timer_handle_t tach_timeout_timer;
static void tach_timeout_handler(void *arg);
static void IRAM_ATTR tach_any_edge_handler(void *arg);

esp_timer_handle_t tach_hold_timer;
static void tach_hold_handler(void *arg);

esp_timer_handle_t time_keeper_timer;
static void time_keeper_handler(void *arg);

void setup_spi_devs()
{
	// Configure the SPI bus
	ESP_LOGD("spi", "Initializing SPI bus");
	spi_bus_config_t bus_cfg = {
		.miso_io_num = MISO_GPIO,
		.mosi_io_num = MOSI_GPIO,
		.sclk_io_num = SCLK_GPIO,
		.quadwp_io_num = -1,
		.quadhd_io_num = -1,
		.max_transfer_sz = 4096,
	};
	ESP_ERROR_CHECK(spi_bus_initialize(HSPI_HOST, &bus_cfg, 1));

	// 2 devices on the SPI bus: the display and FRAM
	ft81x_init(&display, HSPI_HOST, FT81X_CS, FT81X_RST);
}

void setup_adc()
{
	ESP_LOGD("adc", "Initializing ADC");
	// Verify ADC two-point calibration values exist
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
		ESP_LOGV("adc", "eFuse two-point calibration is supported");
	} else {
		ESP_LOGW("adc", "eFuse two-point calibration is NOT supported");
	}

	// Verify ADC Vref calibration value exists
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
		ESP_LOGV("adc", "eFuse Vref calibration is supported");
	} else {
		ESP_LOGW("adc", "eFuse Vref calibration is NOT supported");
	}

	// Tie Vref into an external pin
	ESP_ERROR_CHECK(adc2_vref_to_gpio(VREF_PIN));
}

void setup_display()
{
	ESP_LOGD("disp", "Initializing display");
	gui_init(&display, cmd_buf);
}

void setup_tachometer()
{
	ESP_LOGD("tach", "Initializing tachometer timer and IO");

	const esp_timer_create_args_t timer_args = {
		.callback = tach_timeout_handler,
	};
	ESP_ERROR_CHECK(esp_timer_create(&timer_args, &tach_timeout_timer));

	gpio_config_t tach_conf;
	tach_conf.intr_type = GPIO_PIN_INTR_ANYEDGE; // Both edges required for troubleshooting screen
	tach_conf.mode = GPIO_MODE_INPUT;
	tach_conf.pin_bit_mask = 1ULL << TACH_SIGNAL;
	tach_conf.pull_up_en = 1;
	tach_conf.pull_down_en = 0;
	gpio_config(&tach_conf);

	gpio_isr_handler_add(TACH_SIGNAL, tach_any_edge_handler, NULL);

	const esp_timer_create_args_t args = {
		.callback = tach_hold_handler,
	};
	ESP_ERROR_CHECK(esp_timer_create(&args, &tach_hold_timer));
	ESP_ERROR_CHECK(
		esp_timer_start_periodic(tach_hold_timer, 1000000L / 8L));
}

void setup_time_keeper()
{
	ESP_LOGD("time", "Initializing timers");

	const esp_timer_create_args_t args = {
		.callback = time_keeper_handler,
	};
	ESP_ERROR_CHECK(esp_timer_create(&args, &time_keeper_timer));
	ESP_ERROR_CHECK(esp_timer_start_periodic(time_keeper_timer, 1000000));
}

void heartbeat_task(void *pvParameter)
{
	ESP_LOGD("heartbeat", "Starting heartbeat thread");

	gpio_pad_select_gpio(BLINK_GPIO);
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
	uint32_t i = 0;
	while (1) {
		gpio_set_level(BLINK_GPIO, 1);
		vTaskDelay(50 / portTICK_PERIOD_MS);
		gpio_set_level(BLINK_GPIO, 0);
		vTaskDelay(150 / portTICK_PERIOD_MS);
		gpio_set_level(BLINK_GPIO, 1);
		vTaskDelay(50 / portTICK_PERIOD_MS);
		gpio_set_level(BLINK_GPIO, 0);
		ESP_LOGV("heartbeat", "Beat %u", ++i);
		vTaskDelay(750 / portTICK_PERIOD_MS);
	}
}

void display_update_task(void *pvParameter)
{
	ESP_LOGD("disp", "Starting display thread");

	// Wait for data to be ready
	while (io_data.brightness > 128)
		vTaskDelay(1);

	while (1) {
		gui_update();
		vTaskDelay(1);
	}
}

void brightness_update_task(void *pvParameter)
{
	ESP_LOGD("brightness", "Starting brightness thread");

	static esp_adc_cal_characteristics_t adc_chars;

	// Configure ADC
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(BRIGHTNESS_INPUT, ADC_ATTEN_DB_0);

	// Get calibration values to convert raw value to voltage
	esp_adc_cal_value_t val_type = esp_adc_cal_characterize(
		ADC_UNIT_1, ADC_ATTEN_DB_0, ADC_WIDTH_BIT_12, 1100, &adc_chars);
	switch (val_type) {
	case ESP_ADC_CAL_VAL_EFUSE_VREF:
		ESP_LOGD("brightness", "Using calibrated Vref for ADC");
		break;
	case ESP_ADC_CAL_VAL_EFUSE_TP:
		ESP_LOGD("brightness", "Using two-point calibration for ADC");
		break;
	case ESP_ADC_CAL_VAL_DEFAULT_VREF:
		ESP_LOGW("brightness", "Using uncalibrated Vref for ADC");
		break;
	}

	uint32_t calibrated_brightness;
	
	// If brand new or memory wiped, start out with ridiculous limits.
	if (!fram_data.min_bright_adc && !fram_data.max_bright_adc) {
		fram_data.min_bright_adc = UINT32_MAX;
		fram_data.max_bright_adc = 0;
	}
		
	while (true) {
		{
			uint32_t adc_reading = 0, i = 0;
			for (; i < 64; ++i) { // Average a bunch of samples
				adc_reading += adc1_get_raw(BRIGHTNESS_INPUT);
			}
			adc_reading /= i;
			// Calibrate the average
			calibrated_brightness = esp_adc_cal_raw_to_voltage(adc_reading, &adc_chars);
		}

		lock_fram_data(); // take and give must be around all global variables
		if (fram_data.min_bright_adc > calibrated_brightness) {
			fram_data.min_bright_adc = calibrated_brightness;
		}
		if (fram_data.max_bright_adc < calibrated_brightness) {
			fram_data.max_bright_adc = calibrated_brightness;
		}
		if (fram_data.min_bright_adc == fram_data.max_bright_adc) {
			fram_data.max_bright_adc++; // Make sure they are not the same
		}

		lock_io_data();
		uint32_t scaled_brightness = map(calibrated_brightness, fram_data.min_bright_adc, fram_data.max_bright_adc, 1, 127);
		io_data.brightness = io_data.brightness * 0.75f + scaled_brightness * 0.25f;
		if (io_data.brightness > 127) io_data.brightness = 127;
		unlock_fram_data();

		static uint8_t old_b = 0; // valid brightnesses 0-127
		if (old_b != io_data.brightness) {
			// ESP_LOGD("brightness", "calibrated_brightness=%u scaled_brightness=%u brightness=%u", 
			// 	calibrated_brightness, scaled_brightness, io_data.brightness);
			old_b = io_data.brightness;
		}
		unlock_io_data();

		vTaskDelay(50 / portTICK_PERIOD_MS); // 50 mSec loop
	}
}

void volts_amps_task(void *pvParameter)
{
	ESP_LOGD("v_a", "Starting volts_amps thread");

	static esp_adc_cal_characteristics_t adc_v_chars;
	static esp_adc_cal_characteristics_t adc_alt_chars;
	static esp_adc_cal_characteristics_t adc_bat_chars;

	// Configure ADC
	// #define VOLTAGE_INPUT ADC1_GPIO39_CHANNEL
	// #define ALT_CURRENT_INPUT ADC1_GPIO33_CHANNEL
	// #define BAT_CURRENT_INPUT ADC1_GPIO34_CHANNEL
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(VOLTAGE_INPUT, ADC_ATTEN_DB_0);
	adc1_config_channel_atten(ALT_CURRENT_INPUT, ADC_ATTEN_DB_0);
	adc1_config_channel_atten(BAT_CURRENT_INPUT, ADC_ATTEN_DB_0);

	// Get calibration values to convert raw value to voltage
	esp_adc_cal_value_t v_type   = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_0, ADC_WIDTH_BIT_12, 1100, &adc_v_chars);
	esp_adc_cal_value_t alt_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_0, ADC_WIDTH_BIT_12, 1100, &adc_alt_chars);
	esp_adc_cal_value_t bat_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_0, ADC_WIDTH_BIT_12, 1100, &adc_bat_chars);
	switch (v_type) {
	case ESP_ADC_CAL_VAL_EFUSE_VREF:
		ESP_LOGD("v_a", "Using calibrated Vref for v");
		break;
	case ESP_ADC_CAL_VAL_EFUSE_TP:
		ESP_LOGD("v_a", "Using two-point calibration for v");
		break;
	case ESP_ADC_CAL_VAL_DEFAULT_VREF:
		ESP_LOGW("v_a", "Using uncalibrated Vref for v");
		break;
	}
	switch (alt_type) {
	case ESP_ADC_CAL_VAL_EFUSE_VREF:
		ESP_LOGD("v_a", "Using calibrated Vref for alt");
		break;
	case ESP_ADC_CAL_VAL_EFUSE_TP:
		ESP_LOGD("v_a", "Using two-point calibration for alt");
		break;
	case ESP_ADC_CAL_VAL_DEFAULT_VREF:
		ESP_LOGW("v_a", "Using uncalibrated Vref for alt");
		break;
	}
	switch (bat_type) {
	case ESP_ADC_CAL_VAL_EFUSE_VREF:
		ESP_LOGD("v_a", "Using calibrated Vref for bat");
		break;
	case ESP_ADC_CAL_VAL_EFUSE_TP:
		ESP_LOGD("v_a", "Using two-point calibration for bat");
		break;
	case ESP_ADC_CAL_VAL_DEFAULT_VREF:
		ESP_LOGW("v_a", "Using uncalibrated Vref for bat");
		break;
	}

	while (1) {
		int32_t v_val;
		int32_t alt_val;
		int32_t bat_val;
		{
			uint32_t adc_reading = 0, i = 0;
			for (; i < 64; ++i) { // Average a bunch of samples
				adc_reading += adc1_get_raw(VOLTAGE_INPUT);
			}
			adc_reading /= i;
			// Calibrate the average
			v_val = esp_adc_cal_raw_to_voltage(adc_reading, &adc_v_chars);
		}
		{
			uint32_t adc_reading = 0, i = 0;
			for (; i < 64; ++i) { // Average a bunch of samples
				adc_reading += adc1_get_raw(ALT_CURRENT_INPUT);
			}
			adc_reading /= i;
			// Calibrate the average
			alt_val = esp_adc_cal_raw_to_voltage(adc_reading, &adc_v_chars);
		}
		{
			uint32_t adc_reading = 0, i = 0;
			for (; i < 64; ++i) { // Average a bunch of samples
				adc_reading += adc1_get_raw(BAT_CURRENT_INPUT);
			}
			adc_reading /= i;
			// Calibrate the average
			bat_val = esp_adc_cal_raw_to_voltage(adc_reading, &adc_v_chars);
		}

		// ESP_LOGD("v_a", "V %d A %d B %d", v_val, alt_val, bat_val);

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

void app_main()
{
	state_init(); // needs to be first, to set up semaphores
	gpio_install_isr_service(0);

	// setup_os_status_logging disabled to keep traffic off the USB 10/17/18 Dan
	setup_os_status_logging();

	setup_adc();			// Set up all the ADCs
	setup_spi_devs();		// Must be before any SPI access
	setup_feram();			// Must be after setup_spi_devs, loads all FRAM data
	setup_display();		// Must be after setup_feram, configures the display
	setup_tachometer();		// 
	setup_time_keeper();	//  

	xTaskCreate(&heartbeat_task,         "heartbeat",  configMINIMAL_STACK_SIZE * 3, NULL, 1, NULL);
	xTaskCreate(&brightness_update_task, "brightness", configMINIMAL_STACK_SIZE * 3, NULL, 1, NULL);
	xTaskCreate(&display_update_task,    "display",	   configMINIMAL_STACK_SIZE * 3, NULL, 2, NULL);
	xTaskCreate(&volts_amps_task,        "v_a",        configMINIMAL_STACK_SIZE * 3, NULL, 1, NULL);
	xTaskCreate(&fram_task,              "fram",       configMINIMAL_STACK_SIZE * 3, NULL, 1, NULL);
}

static void tach_timeout_handler(void *arg)
{
	ESP_LOGD("tach", "Idle timeout");
	lock_io_data();
	io_data.rpm_avg = 0;
	unlock_io_data();
}

void any_edge_loader(void *pvParameter1, uint32_t ulParameter2)
{
	lock_io_data();
	if (io_data.tach_mode) { 			// Debugging the tach
		if (tach_data.tach_index < MAX_TACH_SAMPLES) {
			tach_data.tach_delta[tach_data.tach_index] = ulParameter2;
			tach_data.tach_state[tach_data.tach_index] = (uint32_t)pvParameter1;
			// if the buckets are full, go back to normal tach mode
			if (++tach_data.tach_index >= MAX_TACH_SAMPLES) io_data.tach_mode = TACH_MODE_NORMAL;
		}
	}
	else { 								// Normal mode
		static uint32_t _total_rev_time = 0;
		static uint32_t _pulse_counter = 0;

		_total_rev_time += ulParameter2;

		if ((uint32_t)pvParameter1) { 	// Only watch the rising edge for RPMs
			lock_fram_data();
			if (++_pulse_counter >= fram_data.pulses_per_rev) {  // Only go after a full revolution
				_pulse_counter = 0;
				if (_total_rev_time == 0) _total_rev_time = 1; // to prevent div 0
				double _rpm = 60000000.0 / (double)_total_rev_time;

				if (_rpm > fram_data.rpm_max_scale + fram_data.rpm_max_scale) {	// Try to filter out some garbage
					_rpm = fram_data.rpm_max_scale + fram_data.rpm_max_scale;
				} else if (_rpm < 120) {				// Less than 120 RPM is likely stopped.
					_rpm = 0;
				} else {								// Compensate for rounding errors
					_rpm += 1;
				}
				// Compute average RPM on every revolution
				// Note: io_data already locked.
				switch (fram_data.rpm_smoothing) {
					default:
						io_data.rpm_avg = (uint32_t)((double)io_data.rpm_avg * 0.95f + _rpm * 0.05f);
						break;
					case smoothing_low:
						io_data.rpm_avg = (uint32_t)((double)io_data.rpm_avg * 0.98f + _rpm * 0.02f);
						break;
					case smoothing_normal:
						io_data.rpm_avg = (uint32_t)((double)io_data.rpm_avg * 0.95f + _rpm * 0.05f);
						break;
					case smoothing_high:
						io_data.rpm_avg = (uint32_t)((double)io_data.rpm_avg * 0.75f + _rpm * 0.25f);
						break;
				}

				// Bump the tach time (on each revolution)
				lock_calc_data();
				// DISABLED 11/5 - causes UI to lock up
				// fram_data.tach_time += calc_data.tach_time_increment;
				unlock_calc_data();

				_total_rev_time = 0;
			}
			unlock_fram_data();
		}
	}
	unlock_io_data();
}

static void IRAM_ATTR tach_any_edge_handler(void *arg)
{
	static unsigned long _last_rise_time = 0;
	unsigned long _now_time = esp_timer_get_time();
	int _current_state = gpio_get_level(TACH_SIGNAL);

	uint32_t _delta = _now_time - _last_rise_time;

	// Save the start time for the next pulse
	_last_rise_time = _now_time;

	// Restart the watchdog timer
	esp_timer_stop(tach_timeout_timer);
	esp_timer_start_once(tach_timeout_timer, 500000); // 1/2 second timeout (120 RPM minimum)

	// Send the data out of the ISR to a process in the OS
	BaseType_t task_woken = pdFALSE;
	xTimerPendFunctionCallFromISR(any_edge_loader, (void *)_current_state, _delta, &task_woken);
	if (task_woken == pdTRUE) portYIELD_FROM_ISR();
}

static void tach_hold_handler(void *arg)
{
	lock_io_data();
	io_data.rpm_hold = io_data.rpm_avg;
	unlock_io_data();
}

void time_keeper(void *pvParameter1, uint32_t ulParameter2)
{
	lock_io_data();
	lock_fram_data();
	typeof(io_data.rpm_avg) _rpm_avg = io_data.rpm_avg;
	if (_rpm_avg > 120) {
		// DISABLED 11/5 - causes UI to lock up
		// fram_data.hobbs_time += 1000000;	// 1 seconds worth of microseconds
		// ESP_LOGD("time_keeper", "Updated hobbs to %llu.%06llu seconds | tach to %llu.%06llu seconds", 
		// 	fram_data.hobbs_time / 1000000, fram_data.hobbs_time % 1000000, 
		// 	fram_data.tach_time / 1000000, fram_data.tach_time % 1000000);
		io_data.flight += 1000000;			// 1 seconds worth of microseconds
	}
	unlock_io_data();
	unlock_fram_data();
}

static void time_keeper_handler(void *arg)
{
	// Send the data out of the ISR to a process in the OS
	BaseType_t task_woken = pdFALSE;
	xTimerPendFunctionCallFromISR(time_keeper, 0, 0, &task_woken);
	if (task_woken == pdTRUE) portYIELD_FROM_ISR();
}
