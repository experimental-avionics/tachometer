#include "global.h"
#include "os_status.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>

esp_timer_handle_t os_status_timer;
TaskStatus_t os_task_status_list[32];

static void os_status_handler(void *arg);

void setup_os_status_logging()
{
	ESP_LOGD("freertos", "Initializing OS monitoring");
	const esp_timer_create_args_t args = {
		.callback = os_status_handler,
	};
	ESP_ERROR_CHECK(esp_timer_create(&args, &os_status_timer));
	// ESP_ERROR_CHECK(esp_timer_start_periodic(os_status_timer, 5000000));
}

int compare_task_number(const void *a, const void *b)
{
	const UBaseType_t _a = ((const TaskStatus_t *)a)->xTaskNumber;
	const UBaseType_t _b = ((const TaskStatus_t *)b)->xTaskNumber;

	return (_a > _b) - (_a < _b);
}

static void os_status_handler(void *arg)
{
	TaskStatus_t *status = os_task_status_list;
	UBaseType_t count = uxTaskGetSystemState(
		status,
		sizeof(os_task_status_list) / sizeof(*os_task_status_list),
		NULL);

	qsort(status, count, sizeof(TaskStatus_t), compare_task_number);

	ESP_LOGD("freertos", "FreeRTOS task statistics");

	while (count--) {
		char state_buf = '-';
		switch (status->eCurrentState) {
		case eReady:
			state_buf = 'r';
			break;
		case eRunning:
			state_buf = '*';
			break;
		case eBlocked:
			state_buf = 'b';
			break;
		case eSuspended:
			state_buf = 's';
			break;
		case eDeleted:
			state_buf = 'd';
			break;
		}
		ESP_LOGD("freertos",
			 " %-16s id=%-2u state=%c priority=%-2u stack=%u",
			 status->pcTaskName, status->xTaskNumber, state_buf,
			 status->uxCurrentPriority,
			 status->usStackHighWaterMark);
		++status;
	}
}
